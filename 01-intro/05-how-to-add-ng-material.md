# How to Add Angular Material to Your Project

Let's learn how to add Angular Material to an existing Angular project.

In our sample app, Angular Material is already installed. In a project where it
is _not_ installed, you simply need to run:

> `ng add @angular/material`

This command does all the modifications and installs all the necessary
dependencies in your Angular project:

- Installs dependencies in application.json
- Install a pre-built Angular Material theme in `styles.css`.
- Set up base typography and icons for your application in `index.html` (see
  [fonts.google.com/icons](fonts.google.com/icons) for a list of the available
  icons)
- Adds the necessary Angular Material modules to your `app.module.ts` file

## Adding Our First Angular Material Component

Open the `app.component.html` file, and add the following to its template:

```html
<mat-slider min="1" max="100" step="1" value="50">
  <input matSliderThumb />
</mat-slider>
```

This should result in the following being displayed in the browser:

![basic slider](./screenshots/basic-slider.png)
