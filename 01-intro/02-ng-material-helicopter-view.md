# Angular Material in Depth: Helicopter View

In this course, we'll do a deep dive into the **Angular Material** library.

Developing an application in Angular while having to develop **commonly-used
components from scratch** is tedious and takes a **lot of time**. Some UI
elements can take weeks or months to develop!

In addition, developing consistent page styling from scratch is also
time-consuming and **costly**. This is where **Angular Material** comes in!

## What is Angular Material?

Angular Material is a **component library** for Angular that allows you to
quickly give your application a consistent look and feel, with ready-to-use
widgets such as datepickers, inputs, buttons, sliders, and much more. It is the
go-to solution for developing UIs for enterprise Angular apps.

Angular Material is built and maintained by the Angular team at Google.
