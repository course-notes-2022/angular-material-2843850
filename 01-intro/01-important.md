# IMPORTANT

Hello everyone,

Before we start, just two very important things:

**What version of Node should I use?**

The current recommended node version for this course is:

`Node 18`

If you use the command `npm ci` instead of `npm install`, you will use the exact
versions specified on the `package-lock.json` file.

**Where is the Course Code?**

Also, the code repository containing all the course code can be found here:

[Angular Material In Depth GitHub Repository](https://github.com/angular-university/angular-material-course)

Enjoy the course!

Kind Regards, Vasco Angular University
