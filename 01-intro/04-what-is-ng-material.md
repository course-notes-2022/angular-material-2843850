# What is Angular Material?

## What is Material Design

**Material Design** is a design specification created by Google that defines how
a UI should look and feel. Many apps on Android follow the Material Design
specification. It is **not** an implementation of any UI design widgets, just a
**specification** of how these widgets should be defined.

[View the Material Design documentation at material.io/design](material.io/design).

## What is Angular Material?

**Angular Material** is a UI library that **implements** the Material Design
specification. The main goal is to allow developers who are not design
specialists to build attractive, professional-looking UIs. Material contains
simple to complex components. We'll look at a large range of some of the most
common components, as well as some advanced components and custom themes, along
with design principles.

[View the Angular Material documentation at material.angular.io/](https://material.angular.io/)
