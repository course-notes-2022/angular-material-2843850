# Dev Environment Setup

The recommended way to take this course is on a desktop, while **coding along**
with the lessons as we go.

## Github Repo

The course Github repo is available at:

[https://github.com/angular-university/angular-material-course](https://github.com/angular-university/angular-material-course)

The `master` branch contains the **final, completed** version of the code at the
**end** of the course. The `1-start` branch contains the `starting point` code
version at the beginning of the course.

## Installations

We'll need to install the following to complete this course:

- `NodeJs`: Install the `LTS` version (recommended)

- Any IDE of your choice (the course uses WebStorm)

- Git (optional)

- Angular CLI

## Installing the Lesson Code

Clone the `1-start` branch, or download the `.zip` file for the branch from the
Github repo. Open the project in your IDE. Run `git checkout 1-start` to switch
to the correct starting branch (if you cloned the repository).

Install the dependencies using the command `npm ci` to install the **exact
dependencies** specified in the `package-lock.json`.
