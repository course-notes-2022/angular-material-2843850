# Angular Material Virtual Scrolling

Let's learn about **virtual scrolling** in Angular Material.

## What is Virtual Scrolling? When Should We Use It?

**Virtual Scrolling** is useful when we need to scroll through a **large
number** of items in the DOM. Scrolling through so many items using the normal
browser functionality can cause performance issues, as all the items need to be
rendered in the DOM and loaded into memory regardless of whether they're
currently **visible in the scroll container** or not.

### Virtual Scrolling

With Virtual Scrolling, we render **only the elements that are currently
visibile**. We listen for the `scroll` event and **adjust the content of the
scrolling container** based on the scrolling position.

## Implementation

Open the `virtual-scrolling.component.html` file. Refactor as follows:

```html
<div class="container">
  <div class="virtual-scrolling">
    <h3>Virtual Scrolling Demo</h3>

    <mat-list class="mat-elevation-z7">
      <cdk-virtual-scroll-viewport class="scrolling-container" itemSize="48">
        <mat-list-item *cdkVirtualFor="let item of items">
          {{ item }}
        </mat-list-item>
      </cdk-virtual-scroll-viewport>
    </mat-list>
  </div>
</div>
```
