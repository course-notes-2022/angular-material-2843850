# Angular Material Custom Theme

Let's end the course by discussing Angular Material **custom themes**.

Right now, our application is using one of the Angular Material **built-in
themes**, which we can see in the `styles.scss` file:

```scss
@use '@angular/material' as mat;

@import './mixins';

@include responsive-dialogs();

@import '@angular/material/prebuilt-themes/indigo-pink.css'; /* Using built in "indigo-pink* theme */

.mat-calendar-body-cell.highlight-date {
  background-color: red;
}
```

Imagine we are building a dashboard or application that needs to be
**custom-branded** with the colors for our organization. In this case, we build
a **custom theme** in the same way Angular Material builds the **built-in
themes**.

## Implementation

First, `@use` the `@angular/material` styles and remove the built-in theme from
the `styles.scss` file:

```scss
@use '@angular/material' as mat;

@import './mixins';

@include responsive-dialogs();

.mat-calendar-body-cell.highlight-date {
  background-color: red;
}
```

### Composing a Palette

A **palette** is a group of related colors. We can define a **main palette** and
an **accent palette** We can also define a **danger palette** to mark dangerous
operations, though this is rarely used.

Update `styles.scss` as follows:

```scss
@use '@angular/material' as mat;
/* You can add global styles to this file, and also import other style files */

@include mat.core(); // Include the material core styles

// Define theme colors
$dark-primary-text: rgba(black, 0.87);
$light-primary-text: white;

$mat-primary: (
  50: #e8eaf6,
  100: #c5cae9,
  200: #9fa8da,
  300: #7986cb,
  400: #5c6bc0,
  // 500: #3f51b5,
  500: #9f3fb5,
  600: #3949ab,
  700: #303f9f,
  800: #283593,
  900: #1a237e,
  A100: #8c9eff,
  A200: #536dfe,
  A400: #3d5afe,
  A700: #304ff3,
  contrast: (
    50: $dark-primary-text,
    100: $dark-primary-text,
    200: $dark-primary-text,
    300: $light-primary-text,
    400: $light-primary-text,
    500: $light-primary-text,
    600: $light-primary-text,
    700: $light-primary-text,
    800: $light-primary-text,
    900: $light-primary-text,
    A100: $dark-primary-text,
    A200: $light-primary-text,
    A400: $light-primary-text,
    A700: $light-primary-text,
  ),
);

$mat-accent: (
  50: #fce4ec,
  100: #f8bbb0,
  200: #f48fb1,
  300: #f06292,
  400: #ec407a,
  // 500: #e91e63,
  500: #e9d14e,
  600: #d81b60,
  700: #c2185b,
  800: #ad1457,
  900: #880e4f,
  A100: #ff80ab,
  A200: #ff4081,
  A400: #f50057,
  A700: #c51162,
  contrast: (
    50: $dark-primary-text,
    100: $dark-primary-text,
    200: $dark-primary-text,
    300: $light-primary-text,
    400: $light-primary-text,
    500: $light-primary-text,
    600: $light-primary-text,
    700: $light-primary-text,
    800: $light-primary-text,
    900: $light-primary-text,
    A100: $dark-primary-text,
    A200: $light-primary-text,
    A400: $light-primary-text,
    A700: $light-primary-text,
  ),
);

$primary: mat.define-palette($mat-primary);

$accent: mat.define-palette($mat-accent);

$theme: mat.define-light-theme(
  (
    color: (
      primary: $primary,
      accent: $accent,
    ),
    typography: mat.define-typography-config(),
  )
);

@include mat.all-component-themes($theme);

@import './mixins';

@include responsive-dialogs();

.mat-calendar-body-cell.highlight-date {
  background-color: red;
}
```
