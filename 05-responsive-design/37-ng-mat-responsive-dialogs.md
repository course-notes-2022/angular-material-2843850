# Angular Material Responsive Dialogs

Let's now learn how to make **responsive dialogs**.

Dialog elements don't typically work well in small screen sizes. Ideally, we'd
want the dialog to occupy the **full screen** in mobile/smaller screen sizes.

Open `course-dialog.component.ts`. Observe the `openCourseDialog` function,
which we have defined **separately** from the component class:

```ts
export function openEditCourseDialog(dialog: MatDialog, course: Course) {
  const config = new MatDialogConfig();
  config.disableClose = true;
  config.autoFocus = true;
  open;
  config.data = {
    ...course,
  };
  config.panelClass = 'modal-panel'; // Set the `panelClass` config attribute
  const dialogRef = dialog.open(CourseDialogComponent, config);

  return dialogRef.afterClosed();
}
```

Note that the `config` object of class `MatDialogConfig` provides some
**additional attributes** that we can use to customize the styling of the modal:

- `panelClass`: Allows us to add classes to the **modal panel**

- `backdropClass`: Allows us to add classes to the **modal backdrop**

Refactor the global `styles.scss` to use the `responsive-dialogs` mixin defined
in the `_mixins` file:

```scss
@use '@angular/material' as mat;
/* You can add global styles to this file, and also import other style files */

@import './mixins';

@include responsive-dialogs();

@import '@angular/material/prebuilt-themes/indigo-pink.css';

.mat-calendar-body-cell.highlight-date {
  background-color: red;
}
```
