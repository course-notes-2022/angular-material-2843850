# The Angular Material `mat-grid-list` Component

Let's introduce the `mat-grid-list` component. We'll use this component to make
the `CourseCardListComponent` responsive.

`mat-grid-list` is a container component from Angular Material that can be made
from a series of **tiles**. Each tile will be a `CourseCardListComponent`.

## Implementing the `mat-grid-list`

Refactor `courses-card-list.component.html` as follows:

```html
<!--Add a container for your tiles with the `mat-grid-list` directive-->
<mat-grid-list [cols]="3">
  <!--Add a new tile for each course with the `mat-grid-tile` directive-->
  <mat-grid-tile *ngFor="let course of courses">
    <mat-card class="course-card">
      <mat-card-header>
        <mat-card-title>{{ course.description }}</mat-card-title>
      </mat-card-header>
      <img mat-card-image [src]="course.iconUrl" />
      <mat-card-content>
        <p>{{ course.longDescription }}</p>
      </mat-card-content>
      <mat-card-actions class="course-actions">
        <button
          mat-raised-button
          color="primary"
          [routerLink]="['courses', course.id]"
        >
          VIEW COURSE
        </button>
        <button mat-raised-button color="accent" (click)="editCourse(course)">
          EDIT COURSE
        </button>
      </mat-card-actions>
    </mat-card>
  </mat-grid-tile>
</mat-grid-list>
```

Refactor the `max-width` property of the `.courses-panel` element in
`home.component.scss` to allow for the new, wider layout:

```scss
.courses-panel {
  max-width: 1500px;
  margin: 0 auto;
}
```

Save the changes and refresh the browser window. Our new layout has 3 columns
and our courses now appear as tiles in the columns.

This is progress, but this current iteration doesn't look great at smaller
screen sizes.

We'd like to be able to define **different values** for the `cols` attribute
**depending on the size of the screen**. Let's learn to do that in the next
lesson.
