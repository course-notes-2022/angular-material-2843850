# Writing Responsive CSS for Angular Material Components

Let's learn another simple technique for doing responsive design with Angular
Material. We'll learn how to dynamically add/remove **CSS classes** to an
element based on the current screen size.

Add a new class member, `x`, to `CoursesListComponent`:

```ts
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Course } from '../model/course';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { openEditCourseDialog } from '../course-dialog/course-dialog.component';
import { filter } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
@Component({
  selector: 'courses-card-list',
  templateUrl: './courses-card-list.component.html',
  styleUrls: ['./courses-card-list.component.css'],
})
export class CoursesCardListComponent implements OnInit {
  @Input()
  courses: Course[];
  cols = 3;
  rowHeight = '500px';
  handsetPortrait = false; // Add new class member `handsetPortrait`

  constructor(
    private dialog: MatDialog,
    private responsive: BreakpointObserver
  ) {}

  ngOnInit() {
    this.responsive
      .observe([
        Breakpoints.TabletPortrait,
        Breakpoints.TabletLandscape,
        Breakpoints.HandsetPortrait,
        Breakpoints.HandsetLandscape,
      ])
      .subscribe((result) => {
        const breakpoints = result.breakpoints;

        this.cols = 3;
        this.rowHeight = '500px';
        this.handsetPortrait = false; // Set default value if no breakpoints match current screen size

        if (breakpoints[Breakpoints.TabletPortrait]) {
          this.cols = 1;
        } else if (breakpoints[Breakpoints.HandsetPortrait]) {
          this.cols = 1;
          this.rowHeight = '430px';
          this.handsetPortrait = true; // update `handsetPortrait` to true if breakpoint is HandsetPortrait
        } else if (breakpoints[Breakpoints.HandsetLandscape]) {
          this.cols = 1;
        } else if (breakpoints[Breakpoints.TabletLandscape]) {
          this.cols = 2;
        }
      });
  }

  // Omitted...
}
```

Update the `courses-list.component.html` file to use `handsetPortrait` to set
the required class names dynamically:

```html
<mat-grid-list
  [cols]="cols"
  [rowHeight]="rowHeight"
  [ngClass]="{ 'handset-portrait': handsetPortrait }"
>
  <mat-grid-tile *ngFor="let course of courses">
    <mat-card class="course-card">
      <mat-card-header>
        <mat-card-title>{{ course.description }}</mat-card-title>
      </mat-card-header>
      <img mat-card-image [src]="course.iconUrl" />
      <mat-card-content>
        <p>{{ course.longDescription }}</p>
      </mat-card-content>
      <mat-card-actions class="course-actions">
        <button
          mat-raised-button
          color="primary"
          [routerLink]="['courses', course.id]"
        >
          VIEW COURSE
        </button>
        <button mat-raised-button color="accent" (click)="editCourse(course)">
          EDIT COURSE
        </button>
      </mat-card-actions>
    </mat-card>
  </mat-grid-tile>
</mat-grid-list>
```

Finally, add the following style rules in `courses-list.component.css` to apply
at `HandsetPortrait` screen size:

```css
.handset-portrait .course-card {
  width: 100%;
  margin: 0;
  padding: 16px 0;
}

.handset-portrait .course-thumbnail {
  width: 100%;
  margin: 0;
}

.handset-portrait .long-description {
  padding: 10px 10px 0 10px;
}
```
