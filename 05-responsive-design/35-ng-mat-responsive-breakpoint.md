# The Angular Material Responsive Breakpoint Observer Service

Let's learn how to do responsive design without having to write custom CSS media
queries.

The key to doing this is to use the `Breakpoint Observer Service` from Angular
Material. This will allow us to determine **what form factor** our screen is
using, and we can adjust our responsive layout accordingly.

Open the `courses-card-list.component.ts` file, and make the following changes:

```ts
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Course } from '../model/course';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { openEditCourseDialog } from '../course-dialog/course-dialog.component';
import { filter } from 'rxjs/operators';
import { BreakpointObserver } from '@angular/cdk/layout'; // Import `BreakpointObserver`
@Component({
  selector: 'courses-card-list',
  templateUrl: './courses-card-list.component.html',
  styleUrls: ['./courses-card-list.component.css'],
})
export class CoursesCardListComponent implements OnInit {
  @Input()
  courses: Course[];

  constructor(
    private dialog: MatDialog,
    private responsive: BreakpointObserver
  ) {}

  ngOnInit() {
    // The `observe` method returns an Observable
    // to which we can subscribe to receive notifications
    // about the change in screen size
    this.responsive.observe('(max-width: 959px)').subscribe(console.log);
  }

  editCourse(course: Course) {
    // Subscribe to `afterClosed` Observable
    // and receive new values emitted by the
    // dialog
    openEditCourseDialog(this.dialog, course)
      .pipe(filter((val) => !!val))
      .subscribe((val) => console.log('new course value: ', val));
  }
}
```

Save the changes and refresh the browser. Open the console, and resize the
browser window. Note that an object similar to the below is output to the
console:

```js
{
    breakpoints: (max-width: "959px"),
    matches: true
}
```

The `breakpoints` property corresponds to the breakpoints we passed to
`responsive.observe`. The `matches` property tells us whether the current width
of the screen matches the set breakpoint(s). We can grab the breakpoint state
object and determine what features/layout we should show.

## Using Angular Material Pre-defined Breakpoints

We _could_ continue to pass strings to the `responsive.observe` method, but
Material provides a set of **pre-defined breakpoints** for our convenience.

Refactor `courses-card-list.component.ts` to use the pre-configured breakpoints:

```ts
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Course } from '../model/course';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { openEditCourseDialog } from '../course-dialog/course-dialog.component';
import { filter } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
@Component({
  selector: 'courses-card-list',
  templateUrl: './courses-card-list.component.html',
  styleUrls: ['./courses-card-list.component.css'],
})
export class CoursesCardListComponent implements OnInit {
  @Input()
  courses: Course[];
  cols = 3; // add member variable for column count
  rowHeight = '500px'; // add member variable for row height

  constructor(
    private dialog: MatDialog,
    private responsive: BreakpointObserver
  ) {}

  ngOnInit() {
    // The `observe` method returns an Observable
    // to which we can subscribe to receive notifications
    // about the change in screen size
    this.responsive
      .observe([
        Breakpoints.TabletPortrait,
        Breakpoints.TabletLandscape,
        Breakpoints.HandsetPortrait,
        Breakpoints.HandsetLandscape,
      ])
      .subscribe((result) => {
        const breakpoints = result.breakpoints;

        // Set defaul values if no form factors match
        this.cols = 3;
        this.rowHeight = '500px';

        // Only one breakpoint will match the current
        // screensize at a time
        if (breakpoints[Breakpoints.TabletPortrait]) {
          this.cols = 1;
        } else if (breakpoints[Breakpoints.HandsetPortrait]) {
          this.cols = 1;
          this.rowHeight = '430px';
        } else if (breakpoints[Breakpoints.HandsetLandscape]) {
          this.cols = 1;
        } else if (breakpoints[Breakpoints.TabletLandscape]) {
          this.cols = 2;
        }
      });
  }

  // Omitted...
}
```

We've added two new class member attributes to allow us to **dynamically
compute** the number of **columns** and the **row height** we wish to display at
the breakpoints we care about. Let's refactor `courses-care-list.component.html`
to use our new class attributes:

```html
<!--Add `cols` and `rowHeight` members-->
<mat-grid-list [cols]="cols" [rowHeight]="rowHeight">
  <mat-grid-tile *ngFor="let course of courses">
    <mat-card class="course-card">
      <mat-card-header>
        <mat-card-title>{{ course.description }}</mat-card-title>
      </mat-card-header>
      <img mat-card-image [src]="course.iconUrl" />
      <mat-card-content>
        <p>{{ course.longDescription }}</p>
      </mat-card-content>
      <mat-card-actions class="course-actions">
        <button
          mat-raised-button
          color="primary"
          [routerLink]="['courses', course.id]"
        >
          VIEW COURSE
        </button>
        <button mat-raised-button color="accent" (click)="editCourse(course)">
          EDIT COURSE
        </button>
      </mat-card-actions>
    </mat-card>
  </mat-grid-tile>
</mat-grid-list>
```

Save and refresh. Note that our lessons display is now **fully-responsive**!
