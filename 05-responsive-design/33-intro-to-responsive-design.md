# Intro to Responsive Design with Angular Material

Let's learn about responsive design with Angular Material. We'll refactor the
`HomeComponent` to add responsive features.

Currently, our `HomeComponent` contains a single column of cards that is
appropriate for mobile widths, but we are not maximizing the space at larger
screen sizes. We'd also like to modify the design for devices that allow
landscape and portrait modes.

We _could_ achieve this functionality with standard CSS using **media queries**.
However, writing CSS media queries can become difficult to maintain. We'll learn
a more robust solution using Angular Material.

## The `mat-grid-list` Component

We'll be leveraging the `mat-grid-list` component from Angular Material to help
us implement our responsive layout. `mat-grid-list` is a 2D list view that
arranges cells into a grid-based layout.
