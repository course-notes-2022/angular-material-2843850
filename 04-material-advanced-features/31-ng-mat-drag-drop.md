# Angular Material Drag and Drop: Complete Step-by-Step Example

In this module we'll learn to implement **drag and drop** functionality using
Angular Material.

Let's start by adding the nav link to the sidebar navigation for the drag/drop
view. Add the following to `app.component.html`:

```html
<mat-sidenav-container fullscreen>
  <mat-sidenav #sidenav>
    <mat-nav-list (click)="sidenav.close()">
      <a mat-list-item routerLink="/">
        <mat-icon>library_books</mat-icon>
        <span>Courses</span>
      </a>
      <a mat-list-item routerLink="about">
        <mat-icon>question_answer</mat-icon>
        <span>About</span>
      </a>

      <!--Add nav link for drag-drop route-->
      <a mat-list-item routerLink="drag-drop-example">
        <mat-icon>drag_indicator</mat-icon>
        <span>Drag and Drop</span>
      </a>

      <a mat-list-item>
        <mat-icon>person_add</mat-icon>
        <span>Register</span>
      </a>
      <a mat-list-item>
        <mat-icon>account_circle</mat-icon>
        <span>Login</span>
      </a>
    </mat-nav-list>
  </mat-sidenav>

  <!--Omitted...-->
</mat-sidenav-container>
```

Our focus in this lesson will be on implementing functionality to allow the user
to drag and reposition lessons within a list of lessons displayed in the UI.

## Buidling and Displaying the Initial List

Make the following changes to `drag-drop.component.html`:

```html
<div class="drag-drop-container">
  <div class="pending">
    <h3>Lessons To Watch:</h3>

    <!--Note the "cdk" prefix; Angular's internal Component Development Kit-->
    <div cdkDropList class="lessons-list">
      <div class="lesson" *ngFor="let lesson of lessons" cdkDrag>
        {{ lesson.description }}
      </div>
    </div>
  </div>

  <!-- div class="done">

    <h3>Done:</h3>

  </div-->
</div>
```

Save the changes and test in the browser. Note that the list items are now
**draggable**, however we cannot yet **drop** them. We are not yet reacting to
the `drop` event emitted by the dropped element. We want to receive that event,
and then take some action on the list every time we do.

## Reacting to the `cdkDropListDropped` Event

In order to achieve this, we need to listen for the `cdkDropListDropped` event
on the **containing element**, i.e. the `cdkDropList` directive. Let's add a
listener for that event as follows:

```html
<div class="drag-drop-container">
  <div class="pending">
    <h3>Lessons To Watch:</h3>

    <!--Add listener for `cdkDropListDropped` event-->
    <div cdkDropList class="lessons-list" (cdkDropListDropped)="drop($event)">
      <div class="lesson" *ngFor="let lesson of lessons" cdkDrag>
        {{ lesson.description }}
      </div>
    </div>
  </div>

  <!-- div class="done">

    <h3>Done:</h3>

  </div-->
</div>
```

Create a new `drop` method inside `drag-drop.component.ts` that will be called
on the `cdkDropListDropped` event. Note the available methods on the event
object received by the `drop` event handler:

![cdk drag-drop event](./screenshots/cdk-drag-drop-event.png)

- `container`: points to the CdkDropList where the event occurred

- `previousIndex`, `currentIndex`: point to the element that was dragged by the
  user, and where it was dropped

Instead of writing the code to modify our list with this information
**manually**, we'll use an Angular utility method that makes it simple.

Modify `drag-drop.component.ts` as follows:

```ts
import { Component } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Lesson } from '../model/lesson';

@Component({
  selector: 'drag-drop-example',
  templateUrl: 'drag-drop.component.html',
  styleUrls: ['drag-drop.component.scss'],
})
export class DragDropComponent {
  lessons = [
    {
      id: 120,
      description: 'Introduction to Angular Material',
      duration: '4:17',
      seqNo: 1,
      courseId: 11,
    },
    // Omitted...
  ];

  drop(event: CdkDragDrop<Lesson[]>) {
    /*
    `moveItemInArray is an Angular Material utility
    function that makes dragging and dropping items
    rendered from an array simple.
    */
    moveItemInArray(this.lessons, event.previousIndex, event.currentIndex);
  }
}
```

Save changes and test in the browser. Note that we can now **drag and drop** our
lessons!

## Some UI Improvements

Our drag-and-drop is working, but we can make some improvements to the UI.
Currently, there is no clear indication of where the **drop zone** is when
dragging an element.

### Using `SCSS` Mixins

Since we're making use of `SCSS` in our project, we can use **mixins** to help
us style our elements. **Mixins** are small snippets of styles defined in a
`.scss` file that we can import and use in _other_ `.scss` files.

Let's import and use the `_mixins.scss` mixin into our
`drag-drop.component.scss` file as follows:

```scss
@import '../../mixins';

@include drag-drop();

.drag-drop-container {
  font-family: 'Roboto';
  display: flex;
  justify-content: center;
  margin-top: 50px;
}

/* Omitted...*/
```

Note that to use it, we simply **import** the file with the `@import` statement
(**without** the leading `_`), and then `@include` the mixin **by calling it as
a function**.

Make the following updates to `drag-drop.component.html`:

```html
<div class="drag-drop-container">
  <div class="pending">
    <h3>Lessons To Watch:</h3>
    <div
      cdkDropList
      class="lessons-list drag-drop-list"
      (cdkDropListDropped)="drop($event)"
    >
      <div class="lesson" *ngFor="let lesson of lessons" cdkDrag>
        <div class="drop-placeholder" *cdkDragPlaceholder></div>
        {{ lesson.description }}
      </div>
    </div>
  </div>

  <!-- div class="done">

    <h3>Done:</h3>

  </div-->
</div>
```

Note:

- We are adding the `<div>` with the class `drop-placeholder` and the
  `cdkDragPlaceholder` directive **inside** the `cdkDrag` directive.

- We add the `drag-drop-list` class to the `.lessons-list` element.

Save the changes and test in the browser. Note that we're now getting a **clear
indication of the drop zone**, and some nice animations upon drag and drop!
