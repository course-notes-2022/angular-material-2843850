# Angular Material Drag and Drop Between Different Lists

Let's learn how to drag and drop items between **multiple lists** using Angular
Material.

Add the following to `drag-drop.component.html`:

```html
<div class="drag-drop-container">
  <div class="pending">
    <h3>Lessons To Watch:</h3>

    <div
      cdkDropList
      class="lessons-list drag-drop-list"
      (cdkDropListDropped)="drop($event)"
    >
      <div class="lesson" *ngFor="let lesson of lessons" cdkDrag>
        <div class="drop-placeholder" *cdkDragPlaceholder></div>
        {{ lesson.description }}
      </div>
    </div>
  </div>

  <!--Add the second drag-drop element for the "done" lessons-->
  <div class="done">
    <h3>Done:</h3>
    <div
      cdkDropList
      class="lessons-list drag-drop-list"
      (cdkDropListDropped)="drop($event)"
    >
      <div class="lesson" *ngFor="let lesson of done" cdkDrag>
        <div class="drop-placeholder" *cdkDragPlaceholder></div>
        {{ lesson.description }}
      </div>
    </div>
  </div>
</div>
```

Add a new class member, `done`, to the `DragDropComponent`:

```ts
done = [];
```

Note that we cannot drag and drop our lessons yet, because we have **two
separate drag and drop lists**. We need to make our lists part of a **group** of
drag/drop lists. We can do so by applying the `cdkDropListGroup` directive to an
element that **contains the two lists**:

```html
<div class="drag-drop-container" cdkDropListGroup>
  <!--Add `cdkDropListGroup` directive to lists container-->
  <div class="pending">
    <h3>Lessons To Watch:</h3>
    <div
      cdkDropList
      class="lessons-list drag-drop-list"
      (cdkDropListDropped)="drop($event)"
    >
      <div class="lesson" *ngFor="let lesson of lessons" cdkDrag>
        <div class="drop-placeholder" *cdkDragPlaceholder></div>
        {{ lesson.description }}
      </div>
    </div>
  </div>

  <div class="done">
    <h3>Done:</h3>
    <div
      cdkDropList
      class="lessons-list drag-drop-list"
      (cdkDropListDropped)="drop($event)"
    >
      <div class="lesson" *ngFor="let lesson of done" cdkDrag>
        <div class="drop-placeholder" *cdkDragPlaceholder></div>
        {{ lesson.description }}
      </div>
    </div>
  </div>
</div>
```

## Implementing the Functionality to Drag Between Lists

Previously, we looked at the API of the `CdkDragDrop` event object emitted by
the `cdkDropListDropped` event. There are two properties of that event object
we're interested in now:

- `container`
- `previousContainer`

These two properties will allow us to determine if the drag-drop operation
occurred **inside** a list, or **between lists**.

Let's use these two properties in a new class method that will handle
dragging/dropping events, `dropMultiList`:

```ts
import { Component } from '@angular/core';
import {
  CdkDrag,
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Lesson } from '../model/lesson';

@Component({
  selector: 'drag-drop-example',
  templateUrl: 'drag-drop.component.html',
  styleUrls: ['drag-drop.component.scss'],
})
export class DragDropComponent {
  lessons = [
    {
      id: 120,
      description: 'Introduction to Angular Material',
      duration: '4:17',
      seqNo: 1,
      courseId: 11,
    },
    // Omitted...
  ];

  done = [];

  dropMultiList(event: CdkDragDrop<Lesson[]>) {
    if (event.previousContainer === event.container) {
      // Dragging/dropping inside SAME list
      moveItemInArray(this.lessons, event.previousIndex, event.currentIndex);
    } else {
      // Dragging/dropping BETWEEN LISTS
      transferArrayItem(
        event.previousContainer.data, // points to SOURCE array
        event.container.data, // DESTINATION list
        event.previousIndex,
        event.currentIndex
      );
    }
  }
  // Omitted...
}
```

Update `drag-drop.component.html` as follows:

```html
<div class="drag-drop-container" cdkDropListGroup>
  <div class="pending">
    <h3>Lessons To Watch:</h3>
    <!--Add `cdkDropListData` directive and use `dropMultiList` to handle `cdkDropListDropped`-->
    <div
      cdkDropList
      [cdkDropListData]="lessons"
      class="lessons-list drag-drop-list"
      (cdkDropListDropped)="dropMultiList($event)"
    >
      <div class="lesson" *ngFor="let lesson of lessons" cdkDrag>
        <div class="drop-placeholder" *cdkDragPlaceholder></div>
        {{ lesson.description }}
      </div>
    </div>
  </div>

  <div class="done">
    <h3>Done:</h3>
    <!--Add `cdkDropListData` directive and use `dropMultiList` to handle `cdkDropListDropped`-->
    <div
      cdkDropList
      [cdkDropListData]="done"
      class="lessons-list drag-drop-list"
      (cdkDropListDropped)="dropMultiList($event)"
    >
      <div class="lesson" *ngFor="let lesson of done" cdkDrag>
        <div class="drop-placeholder" *cdkDragPlaceholder></div>
        {{ lesson.description }}
      </div>
    </div>
  </div>
</div>
```

Note:

- The `[cdkDropListData]` directive gives us access to the data of EACH of our
  template list inside our `dropMultiList` event handler function. This is
  **required**, otherwise our references to `event.container.data` and
  `event.previousContainer.data` will **break**.
