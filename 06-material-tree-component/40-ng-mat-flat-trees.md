# Angular Material Flat Trees

Let's learn how to implement an Angular Material **Flat Tree**.

The Nested Tree and Flat Tree **look identical on the screen**. The difference
is that the nested tree has its nodes **nested** in the DOM. This is the most
natural way to think about and build a tree.

On the other hand, nesting the nodes might not be a good approach in some use
cases, for example if you need to support **infinite scrolling**, or if your
data is difficult to parse and transform into a nested tree. Styling nested
trees can also sometimes be a problem. In that case, a flat tree might be a good
solution.

With a Flat Tree, **all the nodes are siblings of one another**, with no
nesting.

## Implementing a Flat Tree

Just like with a Nested Tree, we begin by creating a `mat-tree` directive. We
add to it a `[dataSource]` and `[treeControl]`:

```html
<mat-tree class="mat-elevation-z4" [dataSource]="" [treeControl]=""> </mat-tree>
```

We'll use the **same** `TREE_DATA` property for our Flat Tree, but **flatten**
it. To do so we need to define the structure of the flattened node.

### Updating Component Definition

Refactor the `tree-demo.component.ts` file as follows:

```ts
import { Component, OnInit } from '@angular/core';
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
  MatTreeNestedDataSource,
} from '@angular/material/tree';
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';

interface CourseNode {
  name: string;
  children?: CourseNode[];
}

// Create new interface
// describing structure of flat node
interface CourseFlatNode {
  name: string;
  expandable: boolean;
  level: number; // Indicates what level of INDENTING to give to node
}
const TREE_DATA: CourseNode[] = [
  // Omitted...
];

@Component({
  selector: 'tree-demo',
  templateUrl: 'tree-demo.component.html',
  styleUrls: ['tree-demo.component.scss'],
})
export class TreeDemoComponent implements OnInit {
  nestedDataSource = new MatTreeNestedDataSource<CourseNode>();

  nestedTreeControl = new NestedTreeControl<CourseNode>(
    (node) => node.children
  );

  hasNestedChild(index: number, node: CourseNode) {
    return node?.children?.length > 0;
  }

  // Create `hasFlatChild` to pass to `when` function
  // of EXPANDED `matTreeNode` in Flat Tree
  hasFlatChild(index: number, node: CourseFlatNode) {
    return node.expandable;
  }

  // Add flatTreeControl attribute
  flatTreeControl = new FlatTreeControl<CourseFlatNode>(
    (node) => node.level, // pass a function that extracts the LEVEL of node
    (node) => node.expandable // pass a function that determines the EXPANDABILITY of node
  );

  treeFlattener = new MatTreeFlattener(
    (node: CourseNode, level: number): CourseFlatNode => {
      return {
        name: node.name,
        expandable: node.children?.length > 0,
        level,
      };
    }, // function to construct a CourseFlatNode from a given CourseNode
    (node) => node.level, // function to extract the node level
    (node) => node.expandable, // function to extract the node's expandable status
    (node) => node.children // function to extract the node's nested nodes
  );

  flatDataSource = new MatTreeFlatDataSource(
    this.flatTreeControl,
    this.treeFlattener
  );

  ngOnInit() {
    this.nestedDataSource.data = TREE_DATA;
    this.flatDataSource.data = TREE_DATA;
  }
}
```

### Updating Component Template

Refactor `tree-demo.component.html` as follows:

```html
<div class="tree-demo-container">
  <h3>Nested Tree Demo</h3>
  <mat-tree
    [dataSource]="nestedDataSource"
    [treeControl]="nestedTreeControl"
    class="example-tree mat-elevation-z4"
  >
    <!--Define LEAF node-->
    <mat-tree-node *matTreeNodeDef="let node" matTreeNodeToggle>
      {{ node.name }}
    </mat-tree-node>

    <!--Define EXPANDABLE node-->
    <mat-nested-tree-node *matTreeNodeDef="let node; when: hasNestedChild">
      <div class="mat-tree-node">
        <button mat-icon-button matTreeNodeToggle>
          <mat-icon>
            {{ nestedTreeControl.isExpanded(node) ? "expand_more" :
            "chevron_right" }}
          </mat-icon>
        </button>
        {{ node.name }}
      </div>
      <div
        class="nested-node"
        [class.example-tree-invisible]="!nestedTreeControl.isExpanded(node)"
      >
        <ng-container matTreeNodeOutlet></ng-container>
      </div>
    </mat-nested-tree-node>
  </mat-tree>

  <h3>Flat Tree Demo</h3>

  <mat-tree
    class="mat-elevation-z4"
    [dataSource]="flatDataSource"
    [treeControl]="flatTreeControl"
  >
    <!--Define LEAF node-->
    <mat-tree-node *matTreeNodeDef="let node" matTreeNodePadding>
      {{ node.name }}
    </mat-tree-node>

    <!--Define Top-level/Intermediate nodes-->
    <mat-tree-node
      *matTreeNodeDef="let node; when: hasFlatChild"
      matTreeNodePadding
    >
      <button mat-icon-button matTreeNodeToggle>
        <mat-icon>
          {{ nestedTreeControl.isExpanded(node) ? "expand_more" :
          "chevron_right" }}
        </mat-icon>
      </button>
      {{ node.name }}
    </mat-tree-node>
  </mat-tree>
</div>
```

Note that unlike with the Nested Tree, we are **not** using a
`mat-nested-tree-node` for our non-leaf nodes, but instead a second
`mat-tree-node` directive.

Save and refresh. Notice that we now have **two identical trees** displayed on
the screen. The difference is that the **flat tree** is also **flat in the
DOM**:

![nested/flat trees](./screenshots/nested-flat-trees.png)
