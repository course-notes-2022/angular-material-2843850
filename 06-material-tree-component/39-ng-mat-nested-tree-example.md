# Angular Material Nested Tree: Step-by-step Example

In a Nested Tree, the elements are nested in the DOM **inside one another**. The
Nested Tree is **easier to understand** than its alternative, the Flat Tree.
Prefer the **Nested Tree** unless you have a good reason not to!

Open `tree-demo-component.html`, and add the following:

```html
<div class="tree-demo-container">
  <h3>Nested Tree Demo</h3>
  <mat-tree
    [dataSource]="nestedDataSource"
    [treeControl]="nestedTreeControl"
    class="example-tree mat-elevation-z4"
  ></mat-tree>

  <h3>Flat Tree Demo</h3>
</div>
```

Note the following:

- The `[dataSource]` property specifies where the tree gets its **data**

- The `[treeControl]` specifies the control responsible for **expanding and
  collapsing** the nodes of the tree

Create the `nestedDataSource` and `nestedTreeControl` properties in
`tree-demo.component.ts`:

```ts
import { Component, OnInit } from '@angular/core';
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
  MatTreeNestedDataSource,
} from '@angular/material/tree';
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';

interface CourseNode {
  name: string;
  children?: CourseNode[];
}

const TREE_DATA: CourseNode[] = [
  // Omitted...
];

@Component({
  selector: 'tree-demo',
  templateUrl: 'tree-demo.component.html',
  styleUrls: ['tree-demo.component.scss'],
})
export class TreeDemoComponent implements OnInit {
  nestedDataSource = new MatTreeNestedDataSource<CourseNode>();

  /*
  NestedTreeControl() accepts a function
  that extracts the children of a given node
  The node can have any given structure; ensure that
  the function corresponds to the node structure
  */
  nestedTreeControl = new NestedTreeControl<CourseNode>(
    (node) => node.children
  );

  ngOnInit() {
    this.nestedDataSource.data = TREE_DATA;
  }
}
```

## Displaying the Nodes

We'll have two types of nodes in our tree:

- LEAF nodes: Nodes with no children
- EXPANDABLE nodes: Nodes that have children

### Defining Leaf Nodes

To create a **leaf node** we use the `mat-tree-node` directive.

Leaf nodes by default are not expandable. If we want to add that functionality
if the user clicks on it, we need to add the `matTreeNodeToggle` directive.

In order to tell Angular that the **node** needs access to the **data of the
tree**, we need to use the `*matTreeNodeDef` structural directive. We create the
`node` local variable to give us access to the node's data.

We can add the content of the node using Angular interpolation:

```html
<!--Define LEAF node-->
<mat-tree-node *matTreeNodeDef="let node" matTreeNodeToggle>
  {{ node.name }}
</mat-tree-node>
```

### Defining Expandable Nodes

To define an **expandable** node, we need the `mat-nested-tree-node` directive.

Inside the `mat-nested-tree-node`, we add the HTML of our expandable node: a
container div with some basic styles. Inside this div, we create an Angular
Material Icon button to expand/collapse the node with the `matTreeNodeToggle`
directive; this is mandatory to be able to open/close the node.

We display an icon in the button dynamically based on the expanded state of the
node. We check the node state by using the `nestedTreeControl.isExpanded`
function, to which we pass the `node`. To access the node, we again apply the
`*matTreeNodeDef` directive.

### How Does the Tree Know When to Display a LEAF vs an EXPANDED Node?

The Tree knows when to display a leaf node and when to display an expanded node
using the `when` function, that is passed to the `matTreeNodeDef` directive.
`when` accepts a function declaration that returns a boolean. We declare this
function inside our component file.

Declare the following `hasNestedChild` function inside `tree-node.component.ts`.
This is the function we'll pass to `when`:

```ts
  hasNestedChild(index: number, node: CourseNode) {
    return node?.children?.length > 0;
  }
```

The Tree looks for all the **node templates** (defined by the `mat-tree-node` or
`mat-nested-tree-node` directives) and checks the `when` functions of each
template. If `when` returns `true`, the template will be displayed.

If a template **does not have a `when` function**, the template will be
displayed _only if all other `when` functions return false_.

## Implementing Our Nested Tree

Updated `tree-demo.component.html` as follows:

```html
<div class="tree-demo-container">
  <h3>Nested Tree Demo</h3>
  <mat-tree
    [dataSource]="nestedDataSource"
    [treeControl]="nestedTreeControl"
    class="example-tree mat-elevation-z4"
  >
    <!--Define LEAF node-->
    <mat-tree-node *matTreeNodeDef="let node" matTreeNodeToggle>
      {{ node.name }}
    </mat-tree-node>

    <!--Define EXPANDABLE node-->
    <mat-nested-tree-node *matTreeNodeDef="let node; when: hasNestedChild">
      <div class="mat-tree-node">
        <button mat-icon-button matTreeNodeToggle>
          <mat-icon>
            {{ nestedTreeControl.isExpanded(node) ? "expand_more" :
            "chevron_right" }}
          </mat-icon>
        </button>
        {{ node.name }}
      </div>
      <div
        class="nested-node"
        [class.example-tree-invisible]="!nestedTreeControl.isExpanded(node)"
      >
        <ng-container matTreeNodeOutlet></ng-container>
      </div>
    </mat-nested-tree-node>
  </mat-tree>

  <h3>Flat Tree Demo</h3>
</div>
```
