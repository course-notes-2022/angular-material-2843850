# Intro to the Angular Material Tree Component

Let's begin learning about the Angular Material Tree Component. Let's set up the
playground we'll use to demo the component. We'll use the `tree-demo` component
located in the `src/app` folder.

Add the following to `app.component.html`:

```html
<mat-sidenav-container fullscreen>
  <mat-sidenav #sidenav>
    <mat-nav-list (click)="sidenav.close()">
      <a mat-list-item routerLink="/">
        <mat-icon>library_books</mat-icon>
        <span>Courses</span>
      </a>
      <a mat-list-item routerLink="about">
        <mat-icon>question_answer</mat-icon>
        <span>About</span>
      </a>

      <a mat-list-item routerLink="drag-drop-example">
        <mat-icon>drag_indicator</mat-icon>
        <span>Drag and Drop</span>
      </a>

      <!--Add nav link for tree-demo route-->
      <a mat-list-item routerLink="tree-demo">
        <mat-icon>park</mat-icon>
        <span>Tree Demo</span>
      </a>

      <a mat-list-item>
        <mat-icon>person_add</mat-icon>
        <span>Register</span>
      </a>
      <a mat-list-item>
        <mat-icon>account_circle</mat-icon>
        <span>Login</span>
      </a>
    </mat-nav-list>
  </mat-sidenav>

  <!--Omitted...-->
</mat-sidenav-container>
```

Open the `tree-demo.component.ts` file, and note the following `TREE_DATA`
binding:

```ts
const TREE_DATA: CourseNode[] = [
  {
    name: 'Angular For Beginners',
    children: [
      {
        name: 'Introduction to Angular',
      },
      {
        name: 'Angular Component @Input()',
      },
      {
        name: 'Angular Component @Output()',
      },
    ],
  },
  {
    name: 'Angular Material In Depth',
    children: [
      {
        name: 'Introduction to Angular Material',
        children: [
          {
            name: 'Form Components',
          },
          {
            name: 'Navigation and Containers',
          },
        ],
      },
      {
        name: 'Advanced Angular Material',
        children: [
          {
            name: 'Custom Themes',
          },
          {
            name: 'Tree Components',
          },
        ],
      },
    ],
  },
];
```

Note that this is a nested hierarchical data structure, with nodes that can have
an unlimited number of nested child nodes. This type of hierarchical data would
be difficult to represent using an Angular Material Data Table. Let's see how we
can use Angular Material Trees to do so.
