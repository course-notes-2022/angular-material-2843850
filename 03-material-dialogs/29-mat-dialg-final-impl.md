# Angular Material Dialog: Implementation and Demo

Make the following changes to `course-dialog.component.ts`:

```ts
import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { Course } from '../model/course';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'course-dialog',
  templateUrl: './course-dialog.component.html',
  styleUrls: ['./course-dialog.component.css'],
})
export class CourseDialogComponent implements OnInit {
  description: string = '';
  form: FormGroup = this.fb.group({
    description: [this.course.description, Validators.required],
    category: [this.course.category, Validators.required],
    releasedAt: [new Date(), Validators.required],
    longDescription: [this.course.longDescription, Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private course: Course,
    // @Inject decorator specifies what Angular should inject here
    // MAT_DIALOG_DATA always contains the data for
    // the open dialog
    private dialogRef: MatDialogRef<Course> // Get access to the dialog reference
  ) {
    this.description = course.description;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.form.value); // Pass values edited by the dialog to whomever has called the dialog by passing the form value to close()
    // Value will be emitted by `dialogRef.afterClosed()` observable
  }

  ngOnInit() {}
}

// Recommended to create an open dialog function
// SEPARATE from the component definition
// in case you need to open the dialog from
// other parts of the application
export function openEditCourseDialog(dialog: MatDialog, course: Course) {
  const config = new MatDialogConfig();
  config.disableClose = true; // Disables dialog close if user hits <ESCAPE> key
  config.autoFocus = true; // Automatically adds focus when dialog is open
  config.data = {
    ...course,
  };
  const dialogRef = dialog.open(
    CourseDialogComponent, // name of dialog component class we want to open
    config
  );

  return dialogRef.afterClosed(); // Observable that emits after the dialog is closed
}
```

Here, we create a reference to the Dialog that we can leverage both **inside**
the dialog component itself, as well as **outside** the component in other
components that **use** the dialog. We've also implemented logic to **open** and
**close** the dialog.

Make the following updates to `courses-card-list.component.ts`:

```ts
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Course } from '../model/course';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { openEditCourseDialog } from '../course-dialog/course-dialog.component';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'courses-card-list',
  templateUrl: './courses-card-list.component.html',
  styleUrls: ['./courses-card-list.component.css'],
})
export class CoursesCardListComponent implements OnInit {
  @Input()
  courses: Course[];

  constructor(private dialog: MatDialog) {}

  ngOnInit() {}

  editCourse(course: Course) {
    // Subscribe to `afterClosed` Observable
    // and receive new values emitted by the
    // dialog
    openEditCourseDialog(this.dialog, course)
      .pipe(filter((val) => !!val))
      .subscribe((val) => console.log('new course value: ', val));
  }
}
```

Note that we've imported the `openEditCourseDialog` method and leveraged it
inside this component, which will be **responsible for opening the dialog**. We
**subscribe** to the Observable returned by `openEditCourseDialog` and use it to
get the value of the new course from the Dialog modal.

Finally, update `courses-card-list.component.html` to pass the `course` to the
`openEditCourseDialog` method:

```html
<mat-card class="course-card" *ngFor="let course of courses">
  <mat-card-header>
    <mat-card-title>{{ course.description }}</mat-card-title>
  </mat-card-header>
  <img mat-card-image [src]="course.iconUrl" />
  <mat-card-content>
    <p>{{ course.longDescription }}</p>
  </mat-card-content>
  <mat-card-actions class="course-actions">
    <button
      mat-raised-button
      color="primary"
      [routerLink]="['courses', course.id]"
    >
      VIEW COURSE
    </button>
    <button mat-raised-button color="accent" (click)="editCourse(course)">
      <!--pass course here-->
      EDIT COURSE
    </button>
  </mat-card-actions>
</mat-card>
```
