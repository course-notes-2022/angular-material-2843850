# Angular Material Dialogs: How to Create a Dialog

In this section we'll learn about Angular Material `Dialogs`, otherwise known as
modals. Dialogs are a good choice for doing things like asking for confirmation
before doing a dangerous/destructive operation, or implementing your data
modification UI.

Let's look at a simple example of using dialogs to modify our course data.

The Dialog we'll build is Open the `course-dialog.component.html` file. Add the
following content:

```html
<div class="edit-course-form">
  <h2 mat-dialog-title>{{ description }}</h2>
  <mat-dialog-content [formGroup]="form">
    <mat-form-field>
      <input
        matInput
        placeholder="Course Description"
        formControlName="description"
      />
    </mat-form-field>

    <mat-form-field>
      <mat-select placeholder="Select category" formControlName="category">
        <mat-option value="BEGINNER"> Beginner</mat-option>
        <mat-option value="INTERMEDIATE"> Intermediate</mat-option>
        <mat-option value="ADVANCED"> Advanced</mat-option>
      </mat-select>
    </mat-form-field>

    <mat-form-field>
      <input
        matInput
        [matDatepicker]="myDatepicker"
        formControlName="releasedAt"
      />

      <mat-datepicker-toggle matSuffix [for]="myDatepicker">
      </mat-datepicker-toggle>

      <mat-datepicker #myDatepicker></mat-datepicker>
    </mat-form-field>

    <mat-form-field>
      <textarea
        matInput
        placeholder="Description"
        formControlName="longDescription"
      >
      </textarea>
    </mat-form-field>
  </mat-dialog-content>

  <mat-dialog-actions>
    <button mat-raised-button (click)="close()">Close</button>
    <button mat-raised-button color="primary" (click)="save()">Save</button>
  </mat-dialog-actions>
</div>
```

Note that every Material Dialog has the same 3 basic sections:

- a `mat-dialog-title`: The header/title of the dialog
- a `mat-dialog-content`: The content of the dialog
- a `mat-dialog-actions`: Contains the controls for the dialog (e.g. open,
  close)

Add the following content to `course-dialog.component.ts`:

```ts
import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { Course } from '../model/course';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'course-dialog',
  templateUrl: './course-dialog.component.html',
  styleUrls: ['./course-dialog.component.css'],
})
export class CourseDialogComponent implements OnInit {
  description: string = '';
  form: FormGroup = this.fb.group({
    description: ['', Validators.required],
    category: ['BEGINNER', Validators.required],
    releasedAt: [new Date(), Validators.required],
    longDescription: ['', Validators.required],
  });

  constructor(private fb: FormBuilder) {}

  close() {}

  save() {}

  ngOnInit() {}
}
```
