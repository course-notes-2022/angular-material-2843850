# Angular Material Dialog: Best Practices, Patterns and Anti-Patterns

Let's look at some best practices and anti-patterns around Angular Material
Dialogs.

Currently, we're using the dialog for **modifying data**. This is a typical
use-case for Dialogs; our data does not require a lot of fields.

Another common use case for Dialogs is **asking confirmation** before doing
dangerous or destructive operations, such as **deleting** data.

You should not hesitate to use dialogs on **mobile** devices! Later on, we'll
learn how to do this.

## Anti-Patterns

Dialogs are **not** good for use cases when modifying large pieces of data, for
example forms with 20 or more fields. This is because the Dialog would likely
take up the entire screen space and the user would likely have to scroll through
the dialog. In this case a normal form makes more sense.

Another anti-pattern is nesting dialogs within dialogs. It's very confusing to
the user and should be avoided.
