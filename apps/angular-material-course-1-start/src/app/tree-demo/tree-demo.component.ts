import { Component, OnInit } from "@angular/core";
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
  MatTreeNestedDataSource,
} from "@angular/material/tree";
import { FlatTreeControl, NestedTreeControl } from "@angular/cdk/tree";

interface CourseNode {
  name: string;
  children?: CourseNode[];
}

interface CourseFlatNode {
  name: string;
  expandable: boolean;
  level: number; // Indicates what level of INDENTING to give to node
}
const TREE_DATA: CourseNode[] = [
  {
    name: "Angular For Beginners",
    children: [
      {
        name: "Introduction to Angular",
      },
      {
        name: "Angular Component @Input()",
      },
      {
        name: "Angular Component @Output()",
      },
    ],
  },
  {
    name: "Angular Material In Depth",
    children: [
      {
        name: "Introduction to Angular Material",
        children: [
          {
            name: "Form Components",
          },
          {
            name: "Navigation and Containers",
          },
        ],
      },
      {
        name: "Advanced Angular Material",
        children: [
          {
            name: "Custom Themes",
          },
          {
            name: "Tree Components",
          },
        ],
      },
    ],
  },
];

@Component({
  selector: "tree-demo",
  templateUrl: "tree-demo.component.html",
  styleUrls: ["tree-demo.component.scss"],
})
export class TreeDemoComponent implements OnInit {
  nestedDataSource = new MatTreeNestedDataSource<CourseNode>();

  /*
  NestedTreeControl() accepts a function
  that extracts the children of a given node
  The node can have any given structure; ensure that
  the function corresponds to the node structure
  */
  nestedTreeControl = new NestedTreeControl<CourseNode>(
    (node) => node.children
  );

  hasNestedChild(index: number, node: CourseNode) {
    return node?.children?.length > 0;
  }

  hasFlatChild(index: number, node: CourseFlatNode) {
    return node.expandable;
  }

  treeFlattener = new MatTreeFlattener(
    (node: CourseNode, level: number): CourseFlatNode => {
      return {
        name: node.name,
        expandable: node.children?.length > 0,
        level,
      };
    }, // function to construct a CourseFlatNode from a given CourseNode
    (node) => node.level, // function to extract the node level
    (node) => node.expandable, // function to extract the node's expandable status
    (node) => node.children // function to extract the node's nested nodes
  );

  flatTreeControl = new FlatTreeControl<CourseFlatNode>(
    (node) => node.level, // pass a function that extracts the LEVEL of node
    (node) => node.expandable // pass a function that determines the EXPANDABILITY of node
  );

  flatDataSource = new MatTreeFlatDataSource(
    this.flatTreeControl,
    this.treeFlattener
  );

  ngOnInit() {
    this.nestedDataSource.data = TREE_DATA;
    this.flatDataSource.data = TREE_DATA;
  }
}
