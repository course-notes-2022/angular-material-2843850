import { Component, Inject, OnInit, ViewEncapsulation } from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from "@angular/material/dialog";
import { Course } from "../model/course";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import * as moment from "moment";

@Component({
  selector: "course-dialog",
  templateUrl: "./course-dialog.component.html",
  styleUrls: ["./course-dialog.component.css"],
})
export class CourseDialogComponent implements OnInit {
  description: string = "";
  form: FormGroup = this.fb.group({
    description: [this.course.description, Validators.required],
    category: [this.course.category, Validators.required],
    releasedAt: [new Date(), Validators.required],
    longDescription: [this.course.longDescription, Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private course: Course,
    // @Inject decorator specifies what Angular should inject here
    // MAT_DIALOG_DATA always contains the data for
    // the open dialog
    private dialogRef: MatDialogRef<Course> // Get access to the dialog reference //
  ) {
    this.description = course.description;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.form.value); // Pass values edited by the dialog to whomever has called the dialog by passing the form value to close()
    // Value will be emitted by `dialogRef.afterClosed()` observable
  }

  ngOnInit() {}
}

// Recommended to create an open dialog function
// SEPARATE from the component definition
// in case you need to open the dialog from
// other parts of the application
export function openEditCourseDialog(dialog: MatDialog, course: Course) {
  const config = new MatDialogConfig();
  config.disableClose = true; // Disables dialog close if user hits <ESCAPE> key
  config.autoFocus = true; // Automatically adds focus when dialog is open
  config.data = {
    ...course,
  };
  config.panelClass = "modal-panel";

  const dialogRef = dialog.open(
    CourseDialogComponent, // name of dialog component class we want to open
    config
  );

  return dialogRef.afterClosed(); // Observable that emits after the dialog is closed
}
