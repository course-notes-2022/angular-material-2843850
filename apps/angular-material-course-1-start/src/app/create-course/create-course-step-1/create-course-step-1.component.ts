import { Component } from "@angular/core";
import { UntypedFormBuilder, Validators } from "@angular/forms";
import { MatCalendarCellClassFunction } from "@angular/material/datepicker";

const ipsum =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mattis justo at aliquet eleifend. Ut pulvinar neque dolor, at condimentum sapien finibus at. Curabitur ac auctor erat. Phasellus convallis dui lacus, a pharetra nibh condimentum nec. Integer blandit eros ac eleifend congue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce id congue orci, vitae rhoncus ipsum. Quisque aliquam dolor in nibh pharetra, ornare laoreet orci rhoncus. Integer faucibus vestibulum tempus. Nam quis elit eu neque dapibus suscipit. Ut tempus at arcu ut viverra. Cras malesuada dui et magna consectetur blandit.";

@Component({
  selector: "create-course-step-1",
  templateUrl: "create-course-step-1.component.html",
  styleUrls: ["create-course-step-1.component.scss"],
})
export class CreateCourseStep1Component {
  form = this.fb.group({
    title: [
      "",
      [Validators.required, Validators.minLength(5), Validators.maxLength(60)],
    ],
    releasedAt: [new Date(1990, 1, 1), Validators.required],
    category: ["BEGINNER", Validators.required],
    courseType: ["premium", Validators.required],
    downloadsAllowed: [false, Validators.requiredTrue],
    longDescription: [ipsum, [Validators.required, Validators.minLength(3)]],
  });

  constructor(private fb: UntypedFormBuilder) {}

  get courseTitle() {
    return this.form.controls["title"];
  }

  dateClass: MatCalendarCellClassFunction<Date> = (
    cellDate, // Date of the cell
    view // view: month, year, etc...
  ) => {
    const date = cellDate.getDate();
    if (view === "month") {
      return date === 1 ? "highlight-date" : "";
    }
    return "";
  };
}
