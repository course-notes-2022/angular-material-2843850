# Angular Material Data Table Paginator

Let's add some pagination to our data table. We've already displayed our data
table. We'll add a paginator component and integrate it with our data fetching
logic to fetch only the page we need from the backend.

Add the following to `course.component.html`:

```html
<div class="course">
  <h2>{{ course?.description }}</h2>

  <img class="course-thumbnail mat-elevation-z8" [src]="course?.iconUrl" />

  <div *ngIf="loading" class="spinner-container">
    <mat-spinner></mat-spinner>
  </div>

  <table
    mat-table
    class="lessons-table mat-elevation-z8"
    [dataSource]="lessons"
  >
    <ng-container matColumnDef="seqNo">
      <th mat-header-cell *matHeaderCellDef>#</th>
      <td mat-cell *matCellDef="let lesson">{{ lesson.seqNo }}</td>
    </ng-container>

    <ng-container matColumnDef="description">
      <th mat-header-cell *matHeaderCellDef>Description</th>
      <td mat-cell *matCellDef="let lesson">{{ lesson.description }}</td>
    </ng-container>

    <ng-container matColumnDef="duration">
      <th mat-header-cell *matHeaderCellDef>Duration</th>
      <td class="duration-cell" mat-cell *matCellDef="let lesson">
        {{ lesson.duration }}
      </td>
    </ng-container>

    <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
    <tr mat-row *matRowDef="let lesson; columns: displayedColumns"></tr>
  </table>

  <!--Add paginator here-->
  <mat-paginator
    *ngIf="!loading"
    class="mat-elevation-z8"
    [pageSize]="3"
    [pageSizeOptions]="[3, 5, 10]"
    [length]="course?.lessonsCount"
  >
  </mat-paginator>
</div>
```

The above changes result in the following:

![paginator](./screenshots/paginator.png)

The paginator emits a new `PageEvent` whenever the user clicks the forward/back
arrows, or adjusts the results per page. We'll listen for this `PageEvent` and
load the correct results from the backend.

Refactor `course.component.ts` as follows:

```ts
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
// imports omitted...

@Component({
  selector: 'course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
})
export class CourseComponent implements OnInit, AfterViewInit {
  course: Course;
  displayedColumns = ['seqNo', 'description', 'duration'];

  lessons: Lesson[] = [];
  loading = false;

  // Create new `@ViewChild` property
  @ViewChild(MatPaginator) // Pass `MatPaginator` to instruct Angular to grab the first MatPaginator component
  paginator: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private coursesService: CoursesService
  ) {}

  loadLessonsPage() {
    this.loading = true;

    this.coursesService
      .findLessons(
        this.course.id,
        'asc',
        this.paginator?.pageIndex ?? 0, // pass pageIndex and pageSize from paginator to findLessons method
        this.paginator?.pageSize ?? 3
      )
      .pipe(
        tap((lessons) => (this.lessons = lessons)),
        catchError((err) => {
          console.log(err);
          alert('Error loading lessons.');
          return throwError(err);
        }),
        finalize(() => (this.loading = false))
      )
      .subscribe();
  }

  ngOnInit() {
    this.course = this.route.snapshot.data['course'];

    this.loadLessonsPage();
  }

  ngAfterViewInit() {
    // Get the return Observable from loadLessonsPage
    // once the MatPaginator view child is initialized
    this.paginator.page.pipe(tap(() => this.loadLessonsPage())).subscribe();
  }
}
```

Refresh the browser, and note that we are now triggering a new backend request
upon pagination navigation.
