# Angular Material Data Table: Multi-Template Data Rows and Expandable Rows

Let's learn about an advanced feature of Data Tables: Multi-template rows and
expandable rows.

Imagine that we want to expand a row and show additional data if the user clicks
on the row. We can implement this easily with **multi-template** data rows.

## What are Multi-Template Data Rows?

As the name implies, **multi-template** data rows allow us to specify **multiple
templates** for a row. Currently, we are defining a single template for
representing our data rows in our `course.component.html` template:

```html
<tr mat-row *matRowDef="let lesson; columns: displayedColumns"></tr>
```

Refactor the template as follows:

```html
<div class="course">
  <h2>{{ course?.description }}</h2>

  <img class="course-thumbnail mat-elevation-z8" [src]="course?.iconUrl" />

  <div *ngIf="loading" class="spinner-container">
    <mat-spinner></mat-spinner>
  </div>

  <table
    mat-table
    class="lessons-table mat-elevation-z8"
    [dataSource]="lessons"
    matSort
    matSortDisableClear
    matSortActive="seqNo"
    matSortDirection="asc"
    multiTemplateDataRows
  >
    <ng-container matColumnDef="seqNo">
      <th mat-header-cell *matHeaderCellDef mat-sort-header>#</th>
      <td mat-cell *matCellDef="let lesson">{{ lesson.seqNo }}</td>
    </ng-container>

    <ng-container matColumnDef="description">
      <th mat-header-cell *matHeaderCellDef mat-sort-header>Description</th>
      <td mat-cell *matCellDef="let lesson">{{ lesson.description }}</td>
    </ng-container>

    <ng-container matColumnDef="duration">
      <th mat-header-cell *matHeaderCellDef>Duration</th>
      <td class="duration-cell" mat-cell *matCellDef="let lesson">
        {{ lesson.duration }}
      </td>
    </ng-container>

    <!--Add new ng-container with matColumnDef directive-->
    <ng-container matColumnDef="expandedDetail">
      <td mat-cell *matCellDef="let lesson" colspan="3">
        {{ lesson.longDescription }}
      </td>
    </ng-container>

    <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
    <tr mat-row *matRowDef="let lesson; columns: displayedColumns"></tr>
    <!--Define new row template with a SINGLE row-->
    <tr mat-row *matRowDef="let lesson; columns: ['expandedDetail']"></tr>
  </table>
  <mat-paginator
    class="mat-elevation-z8"
    [pageSize]="3"
    [pageSizeOptions]="[3, 5, 10]"
    [length]="course?.lessonsCount"
  >
  </mat-paginator>
</div>
```

Save changes and refresh. Note that we now have a table with **two rows for each
lesson**: one with the previous 3-column display, and another with a single
column containing the lesson **long description**:

![multi-column two rows](./screenshots/multi-column-two-rows.png)

We want to implement the **collapsible** functionality now.

Update `course.component.ts` as follows:

```ts
// Imports omitted...

@Component({
  selector: 'course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
})
export class CourseComponent implements OnInit, AfterViewInit {
  course: Course;
  displayedColumns = ['seqNo', 'description', 'duration'];

  expandedLesson: Lesson = null; // Add the `expandedLesson` class attribute

  lessons: Lesson[] = [];
  loading = false;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  constructor(
    private route: ActivatedRoute,
    private coursesService: CoursesService
  ) {}

  // Add the `onToggleLesson` click handler
  onToggleLesson(lesson: Lesson) {
    if (lesson == this.expandedLesson) {
      this.expandedLesson = null;
    } else {
      this.expandedLesson = lesson;
    }
  }

  // Omitted...
}
```

Update `course.component.html` as follows:

```html
<div class="course">
  <!--Omitted...-->
  <table
    mat-table
    class="lessons-table mat-elevation-z8"
    [dataSource]="lessons"
    matSort
    matSortDisableClear
    matSortActive="seqNo"
    matSortDirection="asc"
    multiTemplateDataRows
  >
    <ng-container matColumnDef="seqNo">
      <th mat-header-cell *matHeaderCellDef mat-sort-header>#</th>
      <td mat-cell *matCellDef="let lesson">{{ lesson.seqNo }}</td>
    </ng-container>

    <ng-container matColumnDef="description">
      <th mat-header-cell *matHeaderCellDef mat-sort-header>Description</th>
      <td mat-cell *matCellDef="let lesson">{{ lesson.description }}</td>
    </ng-container>

    <ng-container matColumnDef="duration">
      <th mat-header-cell *matHeaderCellDef>Duration</th>
      <td class="duration-cell" mat-cell *matCellDef="let lesson">
        {{ lesson.duration }}
      </td>
    </ng-container>

    <!--Add new template with matColumnDef directive-->
    <ng-container matColumnDef="expandedDetail">
      <td mat-cell *matCellDef="let lesson" colspan="3">
        {{ lesson.longDescription }}
      </td>
    </ng-container>

    <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>

    <!--Call `onToggleLesson` on click of each table row; pass current lesson-->
    <tr
      (click)="onToggleLesson(lesson)"
      mat-row
      *matRowDef="let lesson; columns: displayedColumns"
    ></tr>

    <!--Add the `collapsed-detail` class to hide the row if lesson !== expandedLesson-->
    <tr
      mat-row
      *matRowDef="let lesson; columns: ['expandedDetail']"
      [class.collapsed-detail]="lesson != expandedLesson"
    ></tr>
  </table>
  <mat-paginator
    class="mat-elevation-z8"
    [pageSize]="3"
    [pageSizeOptions]="[3, 5, 10]"
    [length]="course?.lessonsCount"
  >
  </mat-paginator>
</div>
```

Add the following style rule to `course.component.css`:

```css
.collapsed-detail {
  display: none;
}
```

Save all changes, and refresh the browser:

![expanded row](./screenshots/expanded-row.png)
