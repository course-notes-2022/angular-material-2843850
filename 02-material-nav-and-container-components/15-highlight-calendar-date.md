# Angular Material Date Picker: Highlighting a Calendar Date

Let's learn a more advanced use case of the Material datepicker: highlighting
dates on the calendar. We want to add a **specific class** to certain dates on
the calendar in order to highlight them. To do so, we need to provide a
**function** to the calendar that determines which cells receive the special
class.

Add the following to `styles.css`:

```css
/**New highlight class */
.highlight-date {
  background: red;
}
```

Update the `create-course-step-1.component.html` file as follows:

```html
<mat-form-field appearance="outline">
  <input
    matInput
    [matDatepicker]="releasedAtPicker"
    formControlName="releasedAt"
    placeholder="Release Date"
  />
  <mat-datepicker-toggle
    matSuffix
    [for]="releasedAtPicker"
  ></mat-datepicker-toggle>
  <mat-datepicker [dateClass]="dateClass" #releasedAtPicker> </mat-datepicker>
</mat-form-field>
```

Add the `dateClass` method to the `create-course-step-1.component.ts` file as
follows:

```ts
import { Component } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { MatCalendarCellClassFunction } from '@angular/material/datepicker';

@Component({
  selector: 'create-course-step-1',
  templateUrl: 'create-course-step-1.component.html',
  styleUrls: ['create-course-step-1.component.scss'],
})
export class CreateCourseStep1Component {
  form = this.fb.group({
    title: [
      '',
      [Validators.required, Validators.minLength(5), Validators.maxLength(60)],
    ],
    releasedAt: [new Date(1990, 1, 1), Validators.required],
    category: ['BEGINNER', Validators.required],
    courseType: ['premium', Validators.required],
    downloadsAllowed: [false, Validators.requiredTrue],
    longDescription: ['', [Validators.required, Validators.minLength(3)]],
  });

  constructor(private fb: UntypedFormBuilder) {}

  get courseTitle() {
    return this.form.controls['title'];
  }

  dateClass: MatCalendarCellClassFunction<Date> = (
    cellDate, // Date of the cell
    view // view: month, year, etc...
  ) => {
    const date = cellDate.getDate();
    if (view === 'month') {
      return date === 1 ? 'highlight-date' : '';
    }
    return '';
  };
}
```

`dateClass` is of type `MatCalendarCellClassFunction`, and simply returns a
string with the name of the class to be applied.
