# Angular Material Tab Groups

Let's continue building our `HomeComponent` view, which we currently see
displayed at the `/` route.

We want to display a **tab group** for beginner and advanced courses. Each tab
group will contain a **card** for each course. We'll use data already being
fetched in the `HomeComponent` to build our lists.

Observe the current state of the `HomeComponent`:

```ts
import { Component, OnInit } from '@angular/core';
import { Course } from '../model/course';
import { Observable } from 'rxjs';
import { CoursesService } from '../services/courses.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  beginnerCourses$: Observable<Course[]>;

  advancedCourses$: Observable<Course[]>;

  constructor(private coursesService: CoursesService) {}

  ngOnInit() {
    const courses$ = this.coursesService.findAllCourses();

    this.beginnerCourses$ = courses$.pipe(
      map((courses) =>
        courses.filter((course) => course.category === 'BEGINNER')
      )
    );

    this.advancedCourses$ = courses$.pipe(
      map((courses) =>
        courses.filter((course) => course.category === 'ADVANCED')
      )
    );
  }
}
```

Note that we define two observables: one for the beginner courses, and one for
the advanced courses. We will consume these in our template.

## Implementing the Tab Group

Add the following to the `home.component.html` file:

```html
<div class="courses-panel">
  <div class="header">
    <h2 class="title">All Courses</h2>

    <button mat-mini-fab routerLink="/add-new-course">
      <mat-icon class="add-course-btn">add</mat-icon>
    </button>
  </div>
  <!--Add the `mat-tab-group` with associated tabs-->
  <mat-tab-group>
    <mat-tab label="Beginners"> </mat-tab>

    <mat-tab label="Advanced"> </mat-tab>
  </mat-tab-group>
</div>
```
