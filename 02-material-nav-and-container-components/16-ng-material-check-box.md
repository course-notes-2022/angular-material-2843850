# Angular Material Check Box

Let's learn about the Material `CheckBox`. Angular Material allows us to display
3 states for a checkbox:

- Checked
- Unchecked
- Indeterminate

The checkbox is similar to the radio group in that we do _not_ need a
`mat-form-field` directive to wrap it.

Add the following to `create-course-step-1.component.html`:

```html
<mat-checkbox color="primary" formControlName="downloadsAllowed">
  Downloads Allowed?
</mat-checkbox>
```

This is all that is needed to create a checkbox! We have a few additional
options we can add:

- `[disabled]`: Checkbox is disabled if `true`, enabled if `false`

- `[indeterminate]`: Sets the checkbox to an "indeterminate" state when form is
  initialized:

![checkbox indeterminate](./screenshots/checkbox-indeterminate)
