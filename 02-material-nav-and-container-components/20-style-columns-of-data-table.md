# How to Style the Columns of an Angular Material Data Table

Let's learn how to properly style Data Tables.

We already have some basic styles applied to our table being added to the cells
and headers of our table. These styles are coming from the style rules we
defined in our `course.component.scss` file:

```css
.mat-column-seqNo {
  width: 32px;
  border-right: 1px solid black;
  padding-right: 24px;
  text-align: center;
}

.mat-column-description {
  padding-left: 20px;
  text-align: left;
}

.mat-column-duration {
  font-style: italic;
}
```

These class names are being generated and applied by Angular Material and
**added automatically** to the appropriate headers and cells. We can style the
elements as we like simply by defining style rules for these generated class
names.
