# Angular Material Data Table: Sticky Header and Sticky Columns

For the final data table lesson, let's add sticky columns and headers to our
table.

When we have a table with lots of elements, we may want to have the header
visible at all times when the user is scrolling on the `y`-axis. We can easily
accomplish this by setting the `sticky: true` directive on the `mat-header-row`:

```html
<tr mat-header-row *matHeaderRowDef="displayedColumns; sticky:true"></tr>
```

If we want to make sure certain **columns** are always visible when scrolling
along the `x`-axis, we can add the `sticky` property to the `ng-container`
column definition to which we want it to apply:

```html
<ng-container matColumnDef="select" sticky>
  <th mat-header-cell *matHeaderCellDef>
    <mat-checkbox
      [checked]="selection.hasValue() && isAllSelected()"
      [indeterminate]="selection.hasValue() && !isAllSelected()"
      (change)="toggleAll()"
    ></mat-checkbox>
  </th>
  <td mat-cell *matCellDef="let lesson" (click)="$event.stopPropagation()">
    <mat-checkbox
      (change)="onLessonToggled(lesson)"
      [checked]="selection.isSelected(lesson)"
    ></mat-checkbox>
  </td>
</ng-container>
```

That's it!
