# Angular Material Cards: How to Use Them

Let's learn to use the Material `Card` component. You'll probably use this
component in most applications you build.

Open the `CoursesCardListComponent`. Note that this is a pure presenational
component.

Add the `CoursesCardListComponent` to the `HomeComponent` template as follows:

```html
<div class="courses-panel">
  <div class="header">
    <h2 class="title">All Courses</h2>

    <button mat-mini-fab routerLink="/add-new-course">
      <mat-icon class="add-course-btn">add</mat-icon>
    </button>
  </div>
  <mat-tab-group>
    <mat-tab label="Beginners">
      <courses-card-list
        [courses]="beginnerCourses$ | async"
      ></courses-card-list>
    </mat-tab>

    <mat-tab label="Advanced">
      <courses-card-list
        [courses]="advancedCourses$ | async"
      ></courses-card-list>
    </mat-tab>
  </mat-tab-group>
</div>
```

This outputs a list of cards for the beginner courses, and one for the advanced
courses.

Refactor the `courses-card-list.component.html` file as follows:

```html
<mat-card class="course-card" *ngFor="let course of courses">
  <mat-card-header>
    <mat-card-title>{{ course.description }}</mat-card-title>
  </mat-card-header>
  <img mat-card-image [src]="course.iconUrl" />
  <mat-card-content>
    <p>{{ course.longDescription }}</p>
  </mat-card-content>
  <mat-card-actions class="course-actions">
    <button
      mat-raised-button
      color="primary"
      [routerLink]="['courses', course.id]"
    >
      VIEW COURSE
    </button>
    <button mat-raised-button color="accent" (click)="editCourse()">
      EDIT COURSE
    </button>
  </mat-card-actions>
</mat-card>
```

The above code builds us a new Material `Card` for each course in the `courses`
prop. View the results in the browser:

![card list](./screenshots/card-list.png)
