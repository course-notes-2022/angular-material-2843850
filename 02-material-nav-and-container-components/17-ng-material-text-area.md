# Angular Material Textarea

Let's learn about the Angular Material `Textarea` component.

The textarea component works a lot like a plain input component that we've
already seen. It's appropriate for longer passages of text.

Add the following to `create-course-step-1.component.html`:

```html
<mat-form-field>
  <textarea
    formControlName="longDescription"
    matInput
    placeholder="Description"
  ></textarea>
</mat-form-field>
```

Note the following:

- We are wrapping the input in a `mat-form-field` element.

- We are placing a plain HTML `textarea` input inside `mat-form-field`, and
  placing the `matInput` directive on the `textarea`.

The above code results in the following output:

![textarea](./screenshots/textarea.png)

We can add the following options to the textarea:

- `cdkTextareaAutosize`: Auto-compute the **height** of the textarea equal to
  the height of the text inside:

![cdk auto resize](./screenshots/cdkautoresize.png)

- `[cdkAutosizeMinRows]="2"`: Set the minimum number of visible rows to 2
- `[cdkAutosizeMaxRows]="5"`: Set the maximum number of visible rows to 5; user
  must scroll to see the rest<sup>1</sup>

```html
<mat-form-field appearance="outline">
  <textarea
    formControlName="longDescription"
    matInput
    placeholder="Description"
    [cdkAutosizeMinRows]="2"
    [cdkAutosizeMaxRows]="5"
  ></textarea>
</mat-form-field>
```

<sup>1</sup>_Note: This does not seem to work in the latest version of Angular
Material. Comments on the video suggest that **removing** the wrapping
`mat-form-field` solves the issue. Investigate?_
