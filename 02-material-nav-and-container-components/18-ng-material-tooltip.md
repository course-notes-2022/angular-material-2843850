# Angular Material Tooltip: Several Useful Options

Let's present a new, useful component for providing extra information to the
user: the **tooltip**.

The browser provides a **native** tooltip option: simply add the `title`
attribute to an element and hover over it. This _could_ be useful, but sometimes
we may want to provide the tooltip _conditionally_ for example. The native
tooltip doesn't give us the ability, but the `matTooltip` directive does!

Add the following to `home.component.html`:

```html
<div class="courses-panel">
  <div class="header">
    <h2 class="title">All Courses</h2>

    <!--Add `matTooltip` directive to button-->
    <button
      mat-mini-fab
      routerLink="/add-new-course"
      matTooltip="Create a Course"
    >
      <mat-icon class="add-course-btn">add</mat-icon>
    </button>
  </div>
  <!--Omitted...-->
</div>
```

![tooltip](./screenshots/tooltip.png)

Note that we can add the following addtional directives to modify a tooltip:

- `matTooltipPosition`: Valid values are [`left`, `right`, `above`, `below`]
- `[matTooltipShowDelay]`: Takes a value in milliseconds as a string
- `[matTooltipHideDelay]`: Takes a value in milliseconds as a string
- `[matTooltipDisabled]`: Disables a tooltip conditionally; accepts a boolean
  value `true` or `false`

```html
<div class="courses-panel">
  <div class="header">
    <h2 class="title">All Courses</h2>

    <button
      mat-mini-fab
      routerLink="/add-new-course"
      matTooltip="Create a Course"
      matTooltipPosition="left"
      [matTooltipShowDelay]="1000"
      [matTooltipHideDelay]="2000"
      [matTooltipDisabled]="true"
    >
      <mat-icon class="add-course-btn">add</mat-icon>
    </button>
  </div>

  <!--Omitted...-->
</div>
```
