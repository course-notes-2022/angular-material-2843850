# Angular Material Data Table: Complete Example with Simplified `DataSource`

For the remainder of this chapter, we'll learn about Angular Material's
`Data Table`.

The `Data Table` is a powerful UI component ideal for displaying **tabular
data** to the user. We'll look at the basices of `Data Table` in this lesson, in
the context of the `CourseComponent`.

Navigate to `/courses`, and click the `View` button for any course. This takes
you to the `/courses/:id` route, which currently looks like this:

![course details start](./screenshots/course-details-start.png)

We'll add a Data Table beneath the course title and thumbnail which displays the
**lessons** associated with the course.

## Modifying the Template

Refactor `course.compenent.html` as follows:

```html
<div class="course">
  <h2>{{ course?.description }}</h2>

  <img class="course-thumbnail mat-elevation-z8" [src]="course?.iconUrl" />

  <!--Add data table-->

  <table
    mat-table
    class="lessons-table mat-elevation-z8"
    [dataSource]="lessons"
  >
    <ng-container matColumnDef="seqNo">
      <th mat-header-cell *matHeaderCellDef>#</th>
      <td mat-cell *matCellDef="let lesson">{{ lesson.seqNo }}</td>
    </ng-container>

    <ng-container matColumnDef="description">
      <th mat-header-cell *matHeaderCellDef>Description</th>
      <td mat-cell *matCellDef="let lesson">{{ lesson.description }}</td>
    </ng-container>

    <ng-container matColumnDef="duration">
      <th mat-header-cell *matHeaderCellDef>Duration</th>
      <td class="duration-cell" mat-cell *matCellDef="let lesson">
        {{ lesson.duration }}
      </td>
    </ng-container>

    <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
    <tr mat-row *matRowDef="let lesson; columns: displayedColumns"></tr>
  </table>
</div>
```

Note the following:

- We pass data to be displayed to our data table as an **array of objects** via
  the `<table [dataSource]="lessons"` directive.

- We **configure a column definition** inside the `<ng-container>` directive.
  Inside this directive, we configure a definition for the **header** of the
  column, as well as for the **table cell**.

  - Add the `matColumnDef` directive to `ng-container`. This directive defines
    the **column that will be defined**.

- The `mat-header-cell` directive applies important CSS classes to the column
  header.

- The `*matHeaderCellDef` marks the template as instantiable by the enclosing
  Material Data Table, i.e. it identifies this as the template of a header cell.

## Modifying Component Definition

Refactor `couse.component.ts` as follows:

```ts
// imports omitted...

@Component({
  selector: 'course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
})
export class CourseComponent implements OnInit, AfterViewInit {
  course: Course;
  displayedColumns = ['seqNo', 'description', 'duration']; // Add `displayedColumns` property
}
```

Refresh the browser window. Your content should look like the following:

![material data table](./screenshots/material-data-table.png)
