# Angular Material Data Table: Loading Data From teh Backend

Let's continue implementing our Data Table. We're currently using **hard-coded
data** for our lessons. Let's refactor to fetch the data from the backend, and
pass it along to our Data Table.

Refactor `course.component.ts` as follows:

```ts
// imports omitted...

import {
  debounceTime,
  distinctUntilChanged,
  startWith,
  tap,
  delay,
  catchError,
} from 'rxjs/operators';
import { merge, fromEvent, throwError } from 'rxjs';
import { Lesson } from '../model/lesson';

@Component({
  selector: 'course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
})
export class CourseComponent implements OnInit, AfterViewInit {
  course: Course;
  displayedColumns = ['seqNo', 'description', 'duration'];

  lessons: Lesson[] = [];

  constructor(
    private route: ActivatedRoute,
    private coursesService: CoursesService
  ) {}

  // Create `loadLessonsPage` method to fetch lessons
  loadLessonsPage() {
    this.coursesService
      .findLessons(this.course.id, 'asc', 0, 3) // `findLessons` returns an Observable
      .pipe(
        tap((lessons) => (this.lessons = lessons)), // set `this.lessons` to value returned by `findLessons`
        catchError((err) => {
          console.log(err);
          alert('Error loading lessons.');
          return throwError(err);
        })
      )
      .subscribe(); // Must subscribe to create stream of values
  }
  ngOnInit() {
    this.course = this.route.snapshot.data['course'];

    this.loadLessonsPage();
  }

  ngAfterViewInit() {}
}
```

![fetch lessons from backend](./screenshots/fetch-lessons-from-backend.png)
