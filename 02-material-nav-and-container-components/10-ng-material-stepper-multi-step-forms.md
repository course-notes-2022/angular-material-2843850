# Angular Material Stepper: Multi-step Forms

In the following lessons, we'll be discussing creating **forms** using Angular
Material components. In this lesson, we'll introduce a new component: `Stepper`.
This will allow us to break our form up into **multiple steps**.

Open the `create-course.component.html` file. Add the following content:

```html
<div class="create-course-panel data-form">
  <h2 class="title">Create New Course</h2>

  <!--Create a horizontal stepper component containing our two-step form-->
  <mat-horizontal-stepper
    class="mat-elevation-z5"
    [linear]="true"
    labelPosition="bottom"
  >
    <!--Add step 1-->
    <mat-step errorMessage="Course details in error">
      <ng-template matStepLabel>Course Details</ng-template>
      <create-course-step-1></create-course-step-1>

      <!--Add controls to advance to next step-->
      <div class="stepper-buttons">
        <button mat-raised-button color="primary" matStepperNext>
          Add Course Lessons
        </button>
      </div>
    </mat-step>

    <!--Add step 2-->
    <mat-step errorMessage="Course lessons in error">
      <ng-template matStepLabel>Course Lessons</ng-template>
      <create-course-step-2></create-course-step-2>
      <!--Add controls to return to previous step-->
      <div class="stepper-buttons">
        <button mat-raised-button color="primary" matStepperPrevious>
          Back
        </button>
        <button mat-raised-button color="accent">Create Course</button>
      </div>
    </mat-step>
  </mat-horizontal-stepper>
</div>
```

The above code creates a horizontal stepper with **two steps**:

1. Add course data: title, category etc.
2. Enter the lessons of the course.

Note the following:

- the `[linear] = "true"` directive means that the stepper is **linear**, i.e.
  step 1 must be completed before step 2 can be started.

- the `matStepperNext` directive links the buton to the stepper and tells the
  stepper to advance to the next step

![stepper](./screenshots/stepper.png)
