# Angular Material Data Table: Data Selection

Let's add the **data selection** functionality to our Data Table. We'll add a
**selection column** that will allow the user to select multiple data rows and
perform operations on them, for example to delete the rows, export them, etc.

## Update `CourseComponent`

Make the following changes to `course.component.ts`:

```ts
// Imports omitted...
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
})
export class CourseComponent implements OnInit, AfterViewInit {
  course: Course;
  displayedColumns = [
    'select', // add new `select` column to displayedColumns
    'seqNo',
    'description',
    'duration',
  ];

  expandedLesson: Lesson = null;

  lessons: Lesson[] = [];
  loading = false;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort)
  sort: MatSort;

  // `selection` represents the list of rows the user
  // has selected from the data table by clicking the
  // row's checkbox
  selection = new SelectionModel<Lesson>(
    true, // allow selection of multiple entries?
    [] // initial user selection
  );

  constructor(
    private route: ActivatedRoute,
    private coursesService: CoursesService
  ) {}

  onLessonToggled(lesson: Lesson) {
    this.selection.toggle(lesson);
    console.log(this.selection.selected);
  }

  isAllSelected() {
    return this.selection.selected?.length === this.lessons?.length;
  }

  toggleAll() {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.selection.select(...this.lessons);
    }
  }

  // Omitted...
}
```

Note the following:

- The `onLessonToggled` method accepts a `Lesson` type parameter, and sets the
  `selected` method of the `selection` class member to the value of the `Lesson`
  parameter

- The `toggleAll` method allows us to select/deselect all the lessons when the
  user changes the value of the "Select All" checkbox

- `isAllSelected` returns `true` if the `selected` array of `this.selection` is
  equal in length to the `lessons` member variable holding all the current
  lessons.

## Update Component Template

Make the following updates to `course.component.html`:

```html
<div class="course">
  <h2>{{ course?.description }}</h2>

  <img class="course-thumbnail mat-elevation-z8" [src]="course?.iconUrl" />

  <div *ngIf="loading" class="spinner-container">
    <mat-spinner></mat-spinner>
  </div>

  <table
    mat-table
    class="lessons-table mat-elevation-z8"
    [dataSource]="lessons"
    matSort
    matSortDisableClear
    matSortActive="seqNo"
    matSortDirection="asc"
    multiTemplateDataRows
  >
    <ng-container matColumnDef="select">
      <th mat-header-cell *matHeaderCellDef>
        <mat-checkbox
          [checked]="selection.hasValue() && isAllSelected()"
          [indeterminate]="selection.hasValue() && !isAllSelected()"
          (change)="toggleAll()"
        ></mat-checkbox>
      </th>
      <td mat-cell *matCellDef="let lesson" (click)="$event.stopPropagation()">
        <mat-checkbox
          (change)="onLessonToggled(lesson)"
          [checked]="selection.isSelected(lesson)"
        ></mat-checkbox>
      </td>
    </ng-container>
    <!--Omitted...-->
</div>

```

Note the following:

- We set the `checked` attribute of the "Select All" `mat-checkbox` to `true` if
  the `selection` class member `hasValue` (i.e., the user has **made a
  selection**) and **all lessons are selected**

- We set the `checked` attribute of the "Select All" `mat-checkbox` to `false`
  if the `selection` class member `hasValue` (i.e., the user has **made a
  selection**) and **all lessons are _not_ selected**

- We add the `toggleAll` method to the "Select All" `mat-checkbox` as a `change`
  event handler

- For the "Select" checkbox in the **data row**, we set the `checked` attribute
  to `true` if the `selection.isSelected` method returns `true` for the row's
  associated lesson

- We add the `onLessonToggled` method as a `change` event handler to set the
  `selected` attribute of the `selecction` class member equal to the row's
  associated lesson

Refresh the browser window, and observe that we should get the desired
select/deselect behavior:

![select all](./screenshots/select-all.png)
