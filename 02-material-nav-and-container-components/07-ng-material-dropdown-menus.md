# Angular Material Dropdown Menus

Let's look at another common navigation element: the **dropdown menu**.

In many desktop applications, we want the navigation items displayed
consistently in the top toolbar. Make the following changes to
`app.component.html`:

```html
<mat-sidenav-container fullscreen>
  <mat-sidenav #sidenav>
    <mat-nav-list (click)="sidenav.close()">
      <a mat-list-item routerLink="/">
        <mat-icon>library_books</mat-icon>
        <span>Courses</span>
      </a>
      <a mat-list-item routerLink="about">
        <mat-icon>question_answer</mat-icon>
        <span>About</span>
      </a>
      <a mat-list-item>
        <mat-icon>person_add</mat-icon>
        <span>Register</span>
      </a>
      <a mat-list-item>
        <mat-icon>account_circle</mat-icon>
        <span>Login</span>
      </a>
    </mat-nav-list>
  </mat-sidenav>

  <mat-toolbar color="primary">
    <button mat-icon-button (click)="sidenav.open()">
      <mat-icon>menu</mat-icon>
    </button>

    <!--Add the toggle button for the dropdown menu-->
    <a mat-button [matMenuTriggerFor]="dropdown"> Screens </a>

    <!--Add the dropdown menu-->
    <mat-menu #dropdown>
      <button mat-menu-item routerLink="/">Courses</button>
      <button mat-menu-item routerLink="about">About</button>
    </mat-menu>
    <a mat-button>
      <mat-icon>person_add</mat-icon>
      Register
    </a>

    <a mat-button>
      <mat-icon>account_circles</mat-icon>
      Login
    </a>
  </mat-toolbar>

  <router-outlet></router-outlet>
</mat-sidenav-container>
```

Note the following:

- We create another **ref** using the `#` notation on the `mat-menu` component

- The `[matMenuTriggerFor]` attribute on the `mat-button` element links the
  **toggle trigger** to the **menu to be toggled**.

Observe the results in the UI:

![top navigation](./screenshots/top-navigation.png)
