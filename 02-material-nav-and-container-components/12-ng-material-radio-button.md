# Angular Material Radio Button

Let's introduce another common Material form component, the radio button.

Adding radio buttons with Angular Material is straightforward. Add the following
to `create-course-step-1.component.html`:

```html
<div [formGroup]="form" class="course-details-form">
  <mat-form-field floatLabel="always">
    <mat-label>Course Title</mat-label>
    <input matInput formControlName="title" #title />
    <mat-hint align="end"> {{ title.value.length }}/60 </mat-hint>
    <mat-error *ngIf="courseTitle.errors?.minLength">
      A minimum length of 5 characters is required
    </mat-error>
  </mat-form-field>

  <!--add radio group and buttons-->
  <mat-radio-group class="course-type" formControlName="courseType">
    <mat-radio-button [disabled]="true" value="free">Free</mat-radio-button>
    <mat-radio-button value="premium">Premium</mat-radio-button>
  </mat-radio-group>
</div>
```

Note that the `mat-radio-group` component does _not_ need to be wrapped in the
`mat-form-field` like an `<input>` does.
