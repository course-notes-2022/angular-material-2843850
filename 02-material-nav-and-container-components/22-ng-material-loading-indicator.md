# Angular Material Loading Indicator

Let's learn to use a loading indicator to indicate to our user that the lesson
data is currently loading.

Adding a loading spinner is as simple as adding the following code to our
template:

```html
<div class="course">
  <h2>{{ course?.description }}</h2>

  <img class="course-thumbnail mat-elevation-z8" [src]="course?.iconUrl" />

  <
  <div class="spinner-container">
    <mat-spinner></mat-spinner>
  </div>

  <!--Table omitted...-->
</div>
```

This will create a loading spinner in our view that is visible **all the time**.
Let's see how we can display it **conditionally**.
