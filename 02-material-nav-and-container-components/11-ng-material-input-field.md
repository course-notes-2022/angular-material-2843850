# Angular Material Input Field: Useful Options

Let's look at some of the most commonly-used Material form controls. We'll start
with the plain text `Input` field.

Let's create a basic plain-text input in `create-course-step-1.component.html`.
Add the following:

```html
<div [formGroup]="form" class="course-details-form">
  <mat-form-field>
    <mat-label>Course Title</mat-label>
    <input matInput formControlName="title" />
  </mat-form-field>
</div>
```

This gives us the following output in the browser:

![basic input](./screenshots/basic-input.png)

Note that we get some nice animation on the `Course Title` label when adding
focus to the input, and some error feedback in the UI **without having to
explicitly write the code for it**. The floating behavior of the label also
makes it convenient to develop for mobile widths.

We can alter the appearance of the input using the `appearance` directive.
`appearance` takes one of two values: `fill` or `outline`:

```html
<div [formGroup]="form" class="course-details-form">
  <mat-form-field>
    <mat-label>Course Title</mat-label>
    <input matInput formControlName="title" />
  </mat-form-field>

  <mat-form-field appearance="fill">
    <mat-label>Course Title</mat-label>
    <input matInput formControlName="title" />
  </mat-form-field>

  <mat-form-field appearance="outline">
    <mat-label>Course Title</mat-label>
    <input matInput formControlName="title" />
  </mat-form-field>
</div>
```

![appearance](./screenshots/input-with-appearance.png)

Note that the default is `fill`.

The `floatLabel` directive controls whether the label "floats" above the input
on focus or not.

See below for more ways to modify a Material Input:

```html
<div [formGroup]="form" class="course-details-form">
  <mat-form-field floatLabel="always">
    <mat-label>Course Title</mat-label>
    <input matInput formControlName="title" #title />
    <!-- <span matPrefix>+1</span> -->
    <!--Add a prefix embedded inside input field-->

    <!-- <span matSuffix>.00</span> -->
    <!--Add a suffix embedded inside input field-->

    <mat-hint align="end"> {{ title.value.length }}/60 </mat-hint>
    <!--Add any supplemental text to the input field-->

    <!-- <mat-error *ngIf="courseTitle.errors?.minLength">
      A minimum length of 5 characters is required
    </mat-error> -->
    <!--Add an error output message to the input-->
  </mat-form-field>
</div>
```
