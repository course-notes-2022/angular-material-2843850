# Angular Material Date Picker

The datepicker is one of the most powerful and configurable components of
Angular Material.

The datepicker is usually made from two parts: an **input field** where the user
can enter the date manually, and a **calendar** where the user can select the
date, month, and year.

Add the following to `create-course-step-1.component.html`:

```html
<div [formGroup]="form" class="course-details-form">
  <!--Omitted...-->
  <mat-form-field appearance="outline">
    <input
      matInput
      [matDatepicker]="releasedAtPicker"
      formControlName="releasedAt"
      placeholder="Release Date"
    />
    <mat-datepicker-toggle
      matSuffix
      [for]="releasedAtPicker"
    ></mat-datepicker-toggle>
    <mat-datepicker #releasedAtPicker> </mat-datepicker>
  </mat-form-field>
</div>
```

The `mat-datepicker` is the calendar overlay element (hidden by default). We
link the `input` element to the `mat-datepicker` by assigning a **reference** to
the `mat-datepicker`, and then assigning the `[matDatepicker]` directive to the
`input` field, setting its value to the reference name. We do the same with the
`[for]` directive on the `mat-datepicker-toggle` element to link it to the
datepicker as well. Now, both the input and the toggle are configured to control
the selected date.

![datepicker](./screenshots/datepicker.png)
