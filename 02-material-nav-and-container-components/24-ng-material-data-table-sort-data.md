# Angular Material Data Table: Sorting Data

Let's learn to implement a **sortable** data table. Add the following to
`course.component.html`:

```html
<div class="course">
  <h2>{{ course?.description }}</h2>

  <img class="course-thumbnail mat-elevation-z8" [src]="course?.iconUrl" />

  <div *ngIf="loading" class="spinner-container">
    <mat-spinner></mat-spinner>
  </div>

  <table
    mat-table
    class="lessons-table mat-elevation-z8"
    [dataSource]="lessons"
    matSort
    matSortDisableClear
    matSortActive="seqNo"
    matSortDirection="asc"
  >
    <!--Add the `mat-sort-header` directive to the `th` element-->
    <ng-container matColumnDef="seqNo">
      <th mat-header-cell *matHeaderCellDef mat-sort-header>#</th>
      <td mat-cell *matCellDef="let lesson">{{ lesson.seqNo }}</td>
    </ng-container>

    <!--Add the `mat-sort-header` directive to the `th` element-->
    <ng-container matColumnDef="description">
      <th mat-header-cell *matHeaderCellDef mat-sort-header>Description</th>
      <td mat-cell *matCellDef="let lesson">{{ lesson.description }}</td>
    </ng-container>

    <ng-container matColumnDef="duration">
      <th mat-header-cell *matHeaderCellDef>Duration</th>
      <td class="duration-cell" mat-cell *matCellDef="let lesson">
        {{ lesson.duration }}
      </td>
    </ng-container>

    <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
    <tr mat-row *matRowDef="let lesson; columns: displayedColumns"></tr>
  </table>
  <mat-paginator
    class="mat-elevation-z8"
    [pageSize]="3"
    [pageSizeOptions]="[3, 5, 10]"
    [length]="course?.lessonsCount"
  >
  </mat-paginator>
</div>
```

Note that we have the following directives available on `mat-table`:

- `matSortDisableClear`: Removes capability for user to disable sorting
- `matSortActive="seqNo"`: Sets default sort column
- `matSortDirection="asc"`: Sets default sort direction

This results in the **UI** changes we want:

![sort table ui styles](./screenshots/sort-table-styles.png)

But if we click the sort indicators, _no data is actually sorted_. We'll
implement that sort behavior next.

## Catching the Sort Event

Let's now catch the `SortEvent` emitted by the `matSort` elements and implement
pagination of our results. Updae `CourseComponent` as follows:

```ts
// imports omitted...

@Component({
  selector: 'course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
})
export class CourseComponent implements OnInit, AfterViewInit {
  course: Course;
  displayedColumns = ['seqNo', 'description', 'duration'];

  lessons: Lesson[] = [];
  loading = false;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  @ViewChild(MatSort) // Create new View Child for matSort element
  sort: MatSort;

  constructor(
    private route: ActivatedRoute,
    private coursesService: CoursesService
  ) {}

  loadLessonsPage() {
    this.loading = true;

    this.coursesService
      .findLessons(
        this.course.id,
        this.sort?.direction ?? 'asc',
        this.paginator?.pageIndex ?? 0,
        this.paginator?.pageSize ?? 3,
        this.sort?.active ?? 'seqNo'
      )
      .pipe(
        tap((lessons) => (this.lessons = lessons)),
        catchError((err) => {
          console.log(err);
          alert('Error loading lessons.');
          return throwError(err);
        }),
        finalize(() => (this.loading = false))
      )
      .subscribe();
  }

  ngOnInit() {
    this.course = this.route.snapshot.data['course'];

    this.loadLessonsPage();
  }

  ngAfterViewInit() {
    /*
    Reset page to 0 (0-indexed) whenever the user changes the sort order
    */
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    // `sortChange is an Observable that emits
    // <Sort> events, which include the new sort
    // order and new sort direction selected by
    // the user

    /*
    Use the `merge` rxjs operator to merge the sortChange and page Observables, such that the loadLessonsPage method will be executed whenever EITHER OBSERVABLE emits a value (i.e., when the user either SORTS the data or SWITCHES TO A NEW PAGE)
    */
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(tap(() => this.loadLessonsPage()))
      .subscribe();
  }
}
```
