# Angular Material Select Box

Let's learn about the Material `Select` component.

Add the following to `create-course-step-1.component.html`:

```html
<div [formGroup]="form" class="course-details-form">
  <!--Omitted...-->

  <mat-form-field appearance="fill">
    <mat-select formControlName="category" placeholder="Select Category">
      <mat-option value="beginners">Beginners</mat-option>
      <mat-option value="INTERMEDIATE">Intermediate</mat-option>
      <mat-option value="ADVANCED">Advanced</mat-option>
    </mat-select>
  </mat-form-field>
</div>
```

Note the output in the browser window:

![basic select box](./screenshots/select-box-basic.png)

## Grouping Options

We can add an option group easily to our `mat-select` component as follows:

```html
<div [formGroup]="form" class="course-details-form">
  <!--Omitted...-->

  <mat-form-field appearance="fill">
    <mat-select formControlName="category" placeholder="Select Category">
      <!--Group options-->
      <mat-optgroup label="Beginners">
        <mat-option value="newtoprogramming">New to Programming</mat-option>
        <mat-option value="newjavascript">New to Javascript</mat-option>
        <mat-option value="newangular">New to Angular</mat-option>
      </mat-optgroup>
      <mat-option value="INTERMEDIATE">Intermediate</mat-option>
      <mat-option value="ADVANCED">Advanced</mat-option>
    </mat-select>
  </mat-form-field>
</div>
```

![option group](./screenshots/mat-opt-group.png)
