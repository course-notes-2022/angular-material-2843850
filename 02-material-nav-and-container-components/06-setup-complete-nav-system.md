# Setup a Complete Naviation System With Angular Material

Let's build the navigation system for our application using Angular Material.
We'll add a top menu bar and an icon that will open a sidebar when clicked.

The best way to add a navigation system is to add it at the **top-level
`AppComponent`**.

Add the following to `app.component.html`:

```html
<mat-sidenav-container>
  <mat-sidenav> </mat-sidenav>

  <mat-toolbar> </mat-toolbar>

  <router-outlet></router-outlet>
</mat-sidenav-container>
```

This gives us the basic structure of our application UI:

- the `mat-sidnav-container` contains everything
- the `mat-sidenav` contains the **navigation**
- the `mat-toolbar` contains the top menu bar
- the `router-outlet` instructs Angular Router where to render the content
  associated with the current route

Continue refactoring the template as follows:

```html
<mat-sidenav-container fullscreen>
  <mat-sidenav #sidenav>
    <!--Obtain a reference to the sidenav element-->
    <mat-nav-list (click)="sidenav.close()">
      <!--Close the sidenav element on click of any of the icons-->
      <a mat-list-item routerLink="/">
        <mat-icon>library_books</mat-icon>
        <span>Courses</span>
      </a>
      <a mat-list-item routerLink="about">
        <mat-icon>question_answer</mat-icon>
        <span>About</span>
      </a>
      <a mat-list-item>
        <mat-icon>person_add</mat-icon>
        <span>Register</span>
      </a>
      <a mat-list-item>
        <mat-icon>account_circle</mat-icon>
        <span>Login</span>
      </a>
    </mat-nav-list>
  </mat-sidenav>

  <mat-toolbar color="primary">
    <button mat-icon-button (click)="sidenav.open()">
      <!--Open the sidenav element on click of the button-->
      <mat-icon>menu</mat-icon>
    </button>
  </mat-toolbar>

  <router-outlet></router-outlet>
</mat-sidenav-container>
```

You should be able to see the following in the browser window:

![sidenav open](./screenshots/sidenav-open.png)
